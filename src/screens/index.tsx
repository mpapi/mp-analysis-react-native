import DashboardComponent from './dashboard-stack/dashboard';
import LoginComponent from './login-stack/login';
import AboutComponent from './profile-stack/about';
import AccessPointChangeComponent from './profile-stack/access-point-change';
import AccessPointListComponent from './profile-stack/access-point-list';
import PasswordChangeComponent from './profile-stack/password-change';
import ProfileComponent from './profile-stack/profile';
import LoadingComponent from './report-stack/loading/LoadingComponent';
import ReportFormBuilderComponent from './report-stack/report-form-builder';
import ReportListComponent from './report-stack/report-list';
import ReportViewerComponent from './report-stack/report-viewer';
import TaskDetailsComponent from './task-stack/task-details';
import TaskListComponent from './task-stack/task-list';

export {
    DashboardComponent as DashboardScreen,
    LoginComponent as LoginScreen,
    AboutComponent as AboutScreen,
    AccessPointChangeComponent as AccessPointChangeScreen,
    AccessPointListComponent as AccessPointListScreen,
    PasswordChangeComponent as PasswordChangeScreen,
    ProfileComponent as ProfileScreen,
    LoadingComponent as LoadingScreen,
    ReportFormBuilderComponent as ReportFormBuilderScreen,
    ReportListComponent as ReportListScreen,
    ReportViewerComponent as ReportViewerScreen,
    TaskDetailsComponent as TaskDetailsScreen,
    TaskListComponent as TaskListScreen
};
