import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {NavigationEvents} from 'react-navigation';

import MpHeader from '../../../components/header';
import {SessionService} from '../../../services/session.service';
import baseStyles from '../../../styles';

export default class PasswordChangeComponent extends Component<any> {

    sessionService: SessionService;

    // state = {
    //     servers: [],
    //     defaultServer: ''
    // };

    constructor(props: any) {
        super(props);

        this.sessionService = SessionService.getInstance();
    }

    didFocus = async () => {

        const user = await this.sessionService.getUser();

        this.setState({servers: user.servers || []});
    };

    render() {
        return (
            <View style={baseStyles.container}>
                <NavigationEvents onDidFocus={this.didFocus}/>
                <MpHeader title='Parola Değiştir'/>
                <View style={[baseStyles.container, baseStyles.center]}>
                    <Text style={{
                        ...baseStyles.title,
                        width: this.props.width - 60,
                        textAlign: 'center'
                    }}>
                        Parola değiştirme ekranımız ilerleyen güncellemelerle uygulamaya dahil edilecektir.
                    </Text>
                </View>
            </View>
        );
    }
}
