import React, {Component} from 'react';
import {Text, View} from 'react-native';

import MpHeader from '../../../components/header';
import baseStyles from '../../../styles';

export default class AboutComponent extends Component<any> {

    render() {
        console.log('this.props:', this.props);

        return (
            <View style={baseStyles.container}>
                <MpHeader title='Hakkında'/>
                <View style={[baseStyles.container, baseStyles.center]}>
                    <Text style={{
                        ...baseStyles.title,
                        width: this.props.width - 60,
                        textAlign: 'center'
                    }}>
                        Sizin için daha kullanışlı uygulama tasarlarken bu sayfayı dolduramadık. :)
                    </Text>
                </View>
            </View>
        );
    }
}
