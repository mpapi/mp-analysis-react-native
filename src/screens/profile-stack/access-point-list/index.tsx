import React, {Component} from 'react';
import {TouchableOpacity, View} from 'react-native';
import {NavigationEvents} from 'react-navigation';

import {MpFlatList} from '../../../components/flat-list';
import {MpListItem} from '../../../components/flat-list/MpListItem';
import MpHeader from '../../../components/header';
import MpIcon from '../../../components/icon/MpIcon';
import {ServerEntity} from '../../../entity/ServerEntity';
import {EndpointService} from '../../../services/endpoint.service';
import baseStyles, {headerButtonStyle, listItemIconContainerStyle} from '../../../styles';

export interface IProps {
    navigation: any;
    t: any;
}

interface IState {
    loading: boolean;
    servers: ServerEntity[];
}

export default class Index extends Component<IProps, IState> {

    endpointService: EndpointService;

    state = {
        loading: false,
        servers: [],
    };

    constructor(props: any) {
        super(props);

        this.endpointService = EndpointService.getInstance();
    }

    didFocus = async () => {
        this.load();
    };

    load = () => {

        this.setState({loading: true}, async () => {
            const servers = await this.endpointService.list();

            this.setState({servers: servers || []});
        });
    };

    gotoChangeAccessPoint = (item?: ServerEntity) => () => {
        const {navigate} = this.props.navigation;

        navigate('AccessPointChange', {item});
    };

    renderItem = ({item, index}: { item: ServerEntity, index: number }) => {

        return (
            <MpListItem
                leftElement={
                    <MpIcon
                        containerStyle={listItemIconContainerStyle}
                        name={item.isDefault ? 'done' : 'remove'}
                        size={18}
                    />
                }
                title={item.title}
                subtitle={item.host}
                index={index}
                chevron
                onPress={this.gotoChangeAccessPoint(item)}
            />
        );
    };

    render() {
        const {servers} = this.state;

        return (
            <View style={baseStyles.container}>
                <NavigationEvents onDidFocus={this.didFocus}/>
                <MpHeader
                    title='Erişim Noktaları'
                    rightComponent={
                        <TouchableOpacity
                            activeOpacity={1}
                            style={headerButtonStyle}
                            onPress={this.gotoChangeAccessPoint()}
                        >
                            <MpIcon
                                name='add'
                            />
                        </TouchableOpacity>
                    }
                />

                <MpFlatList
                    onRefresh={this.load}
                    data={servers || []}
                    renderItem={this.renderItem}
                />
            </View>
        );
    }
}
