import React, {Component} from 'react';
import {Alert, ScrollView, StyleSheet, TouchableOpacity, View} from 'react-native';
import {NavigationEvents} from 'react-navigation';

import MpButton from '../../../components/button/MpButton';
import MpCheckBox from '../../../components/form-elements/MpCheckBox';
import MpInput from '../../../components/form-elements/MpInput';
import MpNumberInput from '../../../components/form-elements/MpNumberInput';
import MpUrlInput from '../../../components/form-elements/MpUrlInput';
import MpHeader from '../../../components/header';
import MpIcon from '../../../components/icon/MpIcon';
import KeyboardShift from '../../../components/KeyboardShift/KeyboardShift';
import {IScreenContextProps} from '../../../contexts/withScreenContext';
import {ServerEntity} from '../../../entity/ServerEntity';
import {EndpointService} from '../../../services/endpoint.service';
import baseStyles, {headerButtonStyle, margin, padding} from '../../../styles';
import {cardShade1} from '../../../styles/shadow';
import {confirm} from '../../../utils/confirm';

export interface IProps extends IScreenContextProps {
}

interface IState {
    loading: boolean;
    server: ServerEntity;
}

export default class AccessPointChangeComponent extends Component<IProps, IState> {

    endpointService: EndpointService;

    state = {
        server: new ServerEntity({}),
        loading: false
    };

    constructor(props: any) {
        super(props);

        this.endpointService = EndpointService.getInstance();
    }

    didFocus = async () => {
        const {getParam} = this.props.navigation;
        const server: ServerEntity = await getParam('item');

        if (server) {
            this.setState({server});
        } else {
            // varsayılan olarak new ServerEntity() oluşturuldu.
        }
    };

    onChangeText = (field: string) => (text: any) => {
        const server = new ServerEntity({
            ...this.state.server,
            [field]: text
        });

        this.setState({server});
    };

    back = () => {
        const {navigation} = this.props;

        navigation.goBack();
    };

    onSave = async () => {

        const {server, loading} = this.state;

        if (loading) {
            return;
        }

        this.setState({loading: true}, async () => {

            try {

                await this.endpointService.save(server);

                this.back();

            } catch (e) {
                console.log('e', e);

                Alert.alert('Hata!', e.message);
            }

            this.setState({loading: false});
        });
    };

    onDelete = async () => {
        const {server} = this.state;

        const result = await confirm('Uyarı', 'Erişim noktasını silmek istediğine emin misin?');

        if (result) {

            await this.endpointService.delete(server.key);

            this.back();
        }

    };

    render() {
        const {t} = this.props;
        const {server, loading} = this.state;

        return (
            <KeyboardShift>
                <View style={baseStyles.container}>
                    <NavigationEvents onDidFocus={this.didFocus}/>
                    <MpHeader
                        title='Erişim Noktasını Düzenle'
                        rightComponent={
                            this.endpointService.has(server.key) &&
                            (
                                <TouchableOpacity
                                    activeOpacity={1}
                                    style={headerButtonStyle}
                                    onPress={this.onDelete}
                                >
                                    <MpIcon
                                        name='trash-o'
                                    />
                                </TouchableOpacity>
                            )
                        }
                    />

                    <ScrollView>
                        <View style={{...margin('16 16 0 16')}}>

                            <View style={styles.card}>
                                <MpInput
                                    label={t('Başlık')}
                                    value={server.title}
                                    onChangeText={this.onChangeText('title')}/>
                                <MpUrlInput
                                    label={t('Host')}
                                    value={server.host}
                                    onChangeText={this.onChangeText('host')}/>
                                <MpNumberInput
                                    label={t('Port')}
                                    value={server.port}
                                    onChangeNumber={this.onChangeText('port')}/>
                                <MpCheckBox
                                    checked={server.isDefault}
                                    title='Varsayılan mı?'
                                    onChange={this.onChangeText('isDefault')}
                                />
                            </View>
                            <MpButton
                                containerStyle={styles.saveButton}
                                title={t('Kaydet')}
                                loading={loading}
                                onPress={this.onSave}/>
                        </View>
                    </ScrollView>
                </View>
            </KeyboardShift>
        );
    }
}

const styles = StyleSheet.create({
    card: {
        minHeight: 228,
        ...cardShade1,
        ...padding('16 16 12 16'),
    },
    saveButton: {
        ...margin('20 56 0 56')
    },
});
