import * as _ from 'lodash';
import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {Avatar, Card, Image, Text} from 'react-native-elements';
import {NavigationEvents} from 'react-navigation';

import {MpFlatList} from '../../../components/flat-list';
import {MpListItem} from '../../../components/flat-list/MpListItem';
import MpHeader from '../../../components/header';
import {MpLoader} from '../../../components/loader';
import {UserEntity} from '../../../entity/UserEntity';
import {UserService} from '../../../services/api/user.service';
import {SessionService} from '../../../services/session.service';
import baseStyles, {darkGrey, grey, headerButtonStyle, margin, primary, white} from '../../../styles';
import {cardShade2} from '../../../styles/shadow';
import {confirm} from '../../../utils/confirm';

const passwordMenuItem = {
    title: 'Şifre Değiştir',
    navTo: 'PasswordChange',
};

const accessPointdMenuItem = {
    title: 'Erişim Noktası',
    navTo: 'AccessPointList',
};

const aboutMenuItem = {
    title: 'Hakkında',
    navTo: 'About',
};

export interface IProps {
    navigation: any;
    t: any;
}

interface IState {
    loading: boolean;
    user: UserEntity;
    menuItems: any[];
}

export default class ProfileComponent extends Component<IProps, IState> {

    sessionService: SessionService;
    userService: UserService;

    state = {
        loading: false,
        user: new UserEntity(),
        menuItems: [],
    };

    constructor(props: any) {
        super(props);

        this.sessionService = SessionService.getInstance();
        this.userService = new UserService(props.navigation);
    }

    didFocus = () => {
        this.load();
    };

    load = async () => {
        const user = await this.sessionService.getUser();

        const menuItems = [];

        if (!!_.result(user, 'permission.mpAnalysis.changePassword')) {
            menuItems.push(passwordMenuItem);
        }

        if (!!_.result(user, 'permission.mpAnalysis.changeDefaultEndpoint')) {
            menuItems.push(accessPointdMenuItem);
        }

        menuItems.push(aboutMenuItem);

        this.setState({user, menuItems});
    };

    onLogout = async () => {

        const isYes = await confirm('Uyarı!', 'Çıkış yapmak istiyor musunuz?');

        if (isYes) {

            this.setState({loading: true}, async () => {

                try {
                    await this.userService.logout();

                } catch (e) {

                    // skip error.
                }
            });

        }
    };

    goto = (navTo: string) => () => this.props.navigation.navigate(navTo);

    renderItem = (props: any) => {
        const {item, index} = props;
        const {title, navTo} = item;

        return (
            <MpListItem
                title={title}
                index={index}
                chevron
                onPress={this.goto(navTo)}
            />
        );
    };

    render() {

        const {menuItems, user, loading} = this.state;

        const title = this.sessionService.getAvatarText();
        const fullname = this.sessionService.getFullnameOrUsername();
        const email = user ? user.email : 'loading...';

        return (
            <View style={baseStyles.container}>
                <NavigationEvents onDidFocus={this.didFocus}/>
                <MpHeader
                    title='Profil'
                    rightComponent={
                        loading
                            ? <MpLoader/>
                            : <TouchableOpacity
                                activeOpacity={1}
                                style={headerButtonStyle}
                                onPress={this.onLogout}
                            >
                                <Image
                                    source={require('../../../assets/icon/Logout.png')}
                                />
                            </TouchableOpacity>
                    }
                />
                <View>
                    <Card
                        containerStyle={{
                            ...cardShade2,
                            ...margin('80 25 20 25'),
                        }}
                    >
                        <View
                            style={styles.avatarContainer}
                        >
                            <Avatar
                                title={title}
                                rounded
                                size={110}
                                titleStyle={{color: grey}}
                                containerStyle={{borderWidth: 2, borderColor: primary}}
                                overlayContainerStyle={{backgroundColor: white}}
                            />
                        </View>
                        <View style={styles.textContainer}>
                            <Text style={styles.title}>{fullname}</Text>
                            <Text style={styles.subtitle}>{email}</Text>
                        </View>
                    </Card>

                    <MpFlatList
                        data={menuItems}
                        renderItem={this.renderItem}
                    />

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    avatarContainer: {
        marginTop: -70,
        alignItems: 'center'
    },
    textContainer: {
        ...margin('20 0 12 0'),
        alignItems: 'center'
    },
    title: {
        fontFamily: 'Roboto-Bold',
        color: darkGrey,
        fontSize: 20
    },
    subtitle: {
        fontFamily: 'Roboto-Regular',
        color: grey,
        fontSize: 15
    },
});
