import React, {PureComponent} from 'react';
import {StyleSheet, View} from 'react-native';
import {Card, Text} from 'react-native-elements';

import {DashboardService} from '../../../services/api/dashboard.service';
import {padding, white} from '../../../styles';
import {blueCard} from '../../../styles/shadow';

export default class ReportCard extends PureComponent<any> {

    dashboardService: DashboardService;

    state = {
        loading: true,
        totalCategory: 0,
        totalReport: 0,
    };

    constructor(props: any) {
        super(props);

        this.dashboardService = new DashboardService(props.navigation);
    }

    componentDidMount() {
        this.onLoad();
    }

    onLoad = async () => {
        const result = await this.dashboardService.status();

        this.setState({
            totalCategory: result.totalCategory,
            totalReport: result.totalReport,
            loading: false
        });
    };

    render() {
        const {loading, totalCategory, totalReport} = this.state;

        return (
            <Card containerStyle={blueCard}>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, ...padding('10')}}>
                        <Text style={styles.reportCardTitle}>Toplam Kategori</Text>
                        <Text style={styles.reportCardValue}>{loading ? '...' : totalCategory}</Text>
                    </View>
                    <View style={{flex: 1, ...padding('10')}}>
                        <Text style={styles.reportCardTitle}>Toplam Rapor</Text>
                        <Text style={styles.reportCardValue}>{loading ? '...' : totalReport}</Text>
                    </View>
                </View>
            </Card>
        );
    }
}

const styles = StyleSheet.create({
    reportCardTitle: {
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: white
    },
    reportCardValue: {
        fontFamily: 'Roboto-Regular',
        color: white,
        fontSize: 32,
    },
});
