import _ from 'lodash';
import React, {PureComponent} from 'react';
import {StyleSheet, View} from 'react-native';
import {Card, Text} from 'react-native-elements';

import MpChart, {chartPadding} from '../../../components/chart/MpChart';
import {MpLoader} from '../../../components/loader';
import {MpChartEntity} from '../../../entity/MpChartEntity';
import {MpDashboardEntity} from '../../../entity/MpDashboardEntity';
import {MpPresentParamEntity} from '../../../entity/MpPresentParamEntity';
import {ProcessEntity} from '../../../entity/ProcessEntity';
import {ProcessService} from '../../../services/api/process.service';
import baseStyles, {margin, paleGrey, primary} from '../../../styles';
import {cardShade1} from '../../../styles/shadow';

export interface IProps {
    screenWidth: number;
    processService: ProcessService;
    dashboardItem: MpDashboardEntity;
    processList: ProcessEntity[];
}

interface IState {
    loading: boolean;
    errorText: string | boolean;
    data: any[];
    chartEntity?: MpChartEntity;
    presentParam?: MpPresentParamEntity;
}

export default class DashboardItem extends PureComponent<IProps, IState> {

    state = {
        loading: false,
        errorText: false,
        data: [],
        chartEntity: new MpChartEntity(),
        presentParam: new MpPresentParamEntity()
    };

    componentDidMount() {
        this.load();
    }

    load = async () => {
        if (this.state.loading) {
            return;
        }

        this.setState({loading: true});

        const {processService, processList, dashboardItem} = this.props;
        const {processKey, presentParamKey, chartKey} = dashboardItem;

        if (!processKey) {
            return this.setState({errorText: 'İşlem Anahtarı Bulunamadı!', loading: false});
        }

        const processItem: ProcessEntity | undefined = _.find(processList, {processKey});

        if (!processItem) {
            return this.setState({errorText: 'İşlem Bulunamadı!', loading: false});
        }

        const presentParam = _.find(processItem.presentParams, {key: presentParamKey});

        if (!presentParam) {
            return this.setState({errorText: 'Tanımlı Parametre Bulunamadı!', loading: false});
        }

        const chartEntity = _.find(processItem.charts, {key: chartKey});

        if (!chartEntity) {
            return this.setState({errorText: 'Tanımlı Grafik Bulunamadı!', loading: false});
        }

        const values = processService.prepareValues(presentParam.params);

        try {
            const data = await processService.run(processKey, [{values}]);
            // TODO datayı kontrol etmek lazım. uygulama patlıyor...

            this.setState({data: data || [], presentParam, chartEntity, loading: false});

        } catch (error) {
            console.log('error: ', error);
            this.setState({errorText: error.message, loading: false});
        }

    };

    renderTitleContainer = () => {
        const {loading, chartEntity} = this.state;

        const title = chartEntity && chartEntity.title;
        const subtitle = chartEntity && chartEntity.subtitle;

        return (
            <View style={styles.titleContainer}>
                {
                    loading
                        ? <Text numberOfLines={1} style={baseStyles.text}>Yükleniyor...</Text>
                        : <>
                            <Text numberOfLines={1} style={baseStyles.text}>{title}</Text>
                            {
                                subtitle &&
                                <Text numberOfLines={1} style={baseStyles.subtext}>{subtitle}</Text>
                            }
                        </>
                }
            </View>
        );
    };

    getChartProps = () => {
        const {screenWidth, dashboardItem} = this.props;
        const {chartEntity, data, errorText} = this.state;

        const rows = dashboardItem.rows || 1;
        const cols = dashboardItem.cols || 1;

        return {
            containerSize: {
                width: (screenWidth * .5 * cols) - (cols * 10) - chartPadding.left - chartPadding.right,
                height: 100 * rows + chartPadding.top + chartPadding.bottom
            },
            errorText,
            chartEntity,
            data
        };
    };

    render() {
        const {loading} = this.state;

        const chartProps = this.getChartProps();

        return (
            <Card containerStyle={cardShade1}>
                <View style={styles.chartContainer}>

                    {
                        loading
                            ? (
                                <View style={{
                                    ...baseStyles.container,
                                    ...baseStyles.center,
                                    ...chartProps.containerSize
                                }}>
                                    <MpLoader/>
                                </View>
                            )
                            : <MpChart {...chartProps}/>
                    }
                </View>
                <View style={styles.line}/>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View style={styles.verticalLine}/>
                    {this.renderTitleContainer()}
                </View>

            </Card>
        );
    }
}

const styles = StyleSheet.create({
    chartContainer: {
        minHeight: 110,
        overflow: 'hidden'
    },
    titleContainer: {
        flex: 1,
        ...margin('4 20'),
    },
    line: {
        borderWidth: 1,
        borderRadius: 10,
        borderColor: paleGrey,
        opacity: .7,
    },
    verticalLine: {
        height: 23,
        width: 2,
        borderWidth: 2,
        borderRadius: 10,
        borderColor: primary,
        marginLeft: -10
    }
});
