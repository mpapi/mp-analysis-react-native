import _ from 'lodash';
import React, {PureComponent} from 'react';
import {Alert, StyleSheet, View} from 'react-native';
import {NavigationEvents} from 'react-navigation';

import MpHeader from '../../../components/header';
import {MpScrollView} from '../../../components/scroll-view';
import {IScreenContextProps} from '../../../contexts/withScreenContext';
import {MpDashboardEntity} from '../../../entity/MpDashboardEntity';
import {ProcessEntity} from '../../../entity/ProcessEntity';
import {ProcessService} from '../../../services/api/process.service';
import {UserService} from '../../../services/api/user.service';
import baseStyles, {margin} from '../../../styles';

import DashboardItem from './DashboardItem';
import ReportCard from './ReportCard';

export interface IProps extends IScreenContextProps {
}

interface IState {
    loading: boolean;
    dashboard: any[];
    processList: ProcessEntity[];
}

export default class DashboardComponent extends PureComponent<IProps, IState> {

    userService: UserService;
    processService: ProcessService;

    state = {
        loading: false,
        dashboard: [],
        processList: []
    };

    constructor(props: any) {
        super(props);

        this.processService = new ProcessService(props.navigation);
        this.userService = new UserService(props.navigation);
    }

    didFocus = () => {

        this.load();
    };

    load = async () => {
        const {loading} = this.state;

        if (loading) {
            return;
        }

        this.setState({loading: true});

        try {

            const processList = await this.processService.list();

            const dashboard = await this.userService.dashboardItems();

            return this.setState({dashboard, loading: false, processList});
        } catch (e) {

            Alert.alert('Hata!', 'Dashboard: ' + e.message);
        }

        this.setState({loading: false});
    };

    renderDashboardItem = (dashboardItem: MpDashboardEntity) => {
        const {width} = this.props;
        const {processList} = this.state;

        return (
            <DashboardItem
                screenWidth={width}
                processService={this.processService}
                dashboardItem={dashboardItem}
                processList={processList}
            />
        );
    };

    renderGridView = () => {
        const {dashboard} = this.state;

        return (
            <View style={styles.dashboardContainer}>
                {
                    dashboard && dashboard.length > 0 &&
                    _.map(dashboard, (columns, rowIndex) => (
                        <View key={'row-' + rowIndex} style={styles.dashboardRow}>
                            {
                                _.map(columns, (dashboardItem: any, columnIndex) => (
                                    <View
                                        key={'column-' + columnIndex}
                                        style={{flex: dashboardItem.cols, ...styles.dashboardColumn}}
                                    >
                                        {
                                            this.renderDashboardItem(dashboardItem)
                                        }
                                    </View>
                                ))
                            }
                        </View>
                    ))
                }
            </View>
        );
    };

    render() {

        const {loading} = this.state;

        return (
            <View style={baseStyles.container}>
                <NavigationEvents onDidFocus={this.didFocus}/>
                <MpHeader title='Dashboard'/>
                <MpScrollView
                    refreshing={!!loading}
                    onRefresh={this.load}
                >

                    <ReportCard/>

                    {
                        !loading &&
                        this.renderGridView()
                    }

                </MpScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    dashboardContainer: {marginBottom: 60},
    dashboardRow: {flexDirection: 'row', justifyContent: 'center'},
    dashboardColumn: {...margin('10 8')}
});
