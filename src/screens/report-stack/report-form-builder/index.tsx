import _ from 'lodash';
import React, {PureComponent} from 'react';
import {Alert, ScrollView, StyleSheet, View} from 'react-native';
import {NavigationEvents, withNavigation} from 'react-navigation';

import MpButton from '../../../components/button/MpButton';
import {MpDateTimeInput, MpDateTimeType} from '../../../components/form-elements/MpDateTimeInput';
import MpInput from '../../../components/form-elements/MpInput';
import MpProcessSelect from '../../../components/form-elements/MpProcessSelect';
import MpSelect from '../../../components/form-elements/MpSelect';
import MpHeader from '../../../components/header';
import {IScreenContextProps} from '../../../contexts/withScreenContext';
import {ProcessEntity} from '../../../entity/ProcessEntity';
import {QueryEntity} from '../../../entity/QueryEntity';
import {QueryParamEntity} from '../../../entity/QueryParamEntity';
import {ProcessService} from '../../../services/api/process.service';
import baseStyles, {margin, padding, white} from '../../../styles';
import {cardShade1} from '../../../styles/shadow';

export interface IProps extends IScreenContextProps {
    navigation: any;
}

interface IState {
    loading: boolean;
    values: { [key: string]: any };
    errors: { [key: string]: any };
    queryItem: QueryEntity;
    processItem: ProcessEntity;
}

class ReportFormBuilderComponent extends PureComponent<IProps, IState> {

    processService: ProcessService;

    state = {
        loading: true,
        values: {},
        errors: {},
        queryItem: new QueryEntity(),
        processItem: new ProcessEntity()
    };

    constructor(props: any) {
        super(props);

        this.processService = new ProcessService(props.navigation);
    }

    didFocus = () => {
        this.load();
    };

    load = () => {
        const {getParam} = this.props.navigation;

        const processItem: ProcessEntity = getParam('processItem');
        const queryItem: QueryEntity | undefined = _.find(processItem.queryItems, {queryKey: 'main'});
        const values = getParam('values') || {};

        if (!queryItem) {
            return Alert.alert('Main Query Bulunamadı!');
        }

        this.setState({
            processItem,
            values,
            queryItem,
            loading: false
        });
    };

    onChange = (field: string) => (value: any) => {
        if (!field) {
            return;
        }

        const {values, errors} = this.state;

        this.setState({
            values: {...values, [field]: value},
            errors: {...errors, [field]: undefined}
        });
    };

    isEmpty = (value: any) => _.isUndefined(value) || (_.isString(value) && _.isEmpty(value));

    checkForm = (): boolean => {
        const {queryItem, values} = this.state;
        const paramNames: string[] = _.map(queryItem.params, (p: QueryParamEntity) => p.name || '');
        const errors: { [key: string]: any } = {};

        for (const paramName of paramNames) {

            const value = _.get(values, paramName);

            if (this.isEmpty(value)) {
                errors[paramName] = 'Boş Geçilmez!';
            }
        }

        if ((_.keys(errors) || []).length === 0) {
            return true;
        }

        this.setState({errors});

        return false;
    };

    gotoReportViewer = () => {
        if (!this.checkForm()) {
            console.log('rapor form doldurulmadı.');
            return;
        }

        const {navigate} = this.props.navigation;
        const {processItem, values} = this.state;

        navigate('ReportViewer', {
            processItem,
            values
        });
    };

    renderForm = () => {
        const {queryItem, values, errors, processItem} = this.state;

        if (!_.isArray(queryItem.params)) {
            return;
        }

        const form: any[] = [];

        // // Test form data
        // queryItem.params = [
        //     {type: 'input', name: 'value1', values: [], title: 'Metin'},
        //     {type: 'date', name: 'tarih1', values: [], title: 'Tarih'},
        //     {type: 'time', name: 'tarih2', values: [], title: 'Saat'},
        //     {type: 'datetime', name: 'tarih3', values: [], title: 'Tarih Saat'},
        //     {
        //         type: 'select',
        //         name: 'cari_no1',
        //         values: [
        //             {text: 'Hakan', value: 0},
        //             {text: 'Yusuf', value: 1},
        //             {text: 'Hasan', value: 2},
        //             {text: 'Ramazan', value: 4},
        //         ],
        //         title: 'Select'
        //     },
        //     {type: 'process', source: 'cari_list', name: 'cari_no2', values: [], title: 'Process'},
        //     {type: 'input', name: 'cari_no3', values: [], title: 'Cari Seçin'},
        //     {type: 'input', name: 'cari_no4', values: [], title: 'Cari Seçin'},
        // ];

        for (let i = 0; i < queryItem.params.length; i++) {
            const queryParam = queryItem.params[i];

            const name: string = queryParam.name;

            const defaultProps = {
                key: `${i}-${queryParam.title}-${queryParam.name}`,
                label: queryParam.title,
                value: _.get(values, name),
                errorMessage: _.get(errors, name),
            };

            switch (queryParam.type) {
                case 'date':
                    form.push(
                        <MpDateTimeInput
                            type={MpDateTimeType.DATE}
                            onChangeDate={this.onChange(name)}
                            {...defaultProps}
                        />
                    );
                    break;
                case 'time':
                    form.push(
                        <MpDateTimeInput
                            type={MpDateTimeType.TIME}
                            onChangeDate={this.onChange(name)}
                            {...defaultProps}
                        />
                    );
                    break;
                case 'datetime':
                    form.push(
                        <MpDateTimeInput
                            type={MpDateTimeType.DATETIME}
                            onChangeDate={this.onChange(name)}
                            {...defaultProps}
                        />
                    );
                    break;
                case 'select':
                    form.push(
                        <MpSelect
                            data={queryParam.values || []}
                            search={false}
                            {...defaultProps}
                            onChangeValue={this.onChange(name)}
                        />
                    );
                    break;
                case 'process':
                    form.push(
                        <MpProcessSelect
                            search
                            {...defaultProps}
                            processKey={processItem.processKey}
                            queryItem={_.find(processItem.queryItems, {queryKey: queryParam.source || 'main'})}
                            onChangeValue={this.onChange(name)}
                        />
                    );
                    break;
                case 'input':
                default:
                    form.push(
                        <MpInput
                            {...defaultProps}
                            onChangeText={this.onChange(name)}
                        />
                    );
            }
        }
        return form;
    };

    render() {
        const {t} = this.props;
        const {loading} = this.state;

        return (
            <View style={baseStyles.container}>
                <NavigationEvents onDidFocus={this.didFocus}/>
                <MpHeader title='Rapor Formu' subtitle={loading && 'Yükleniyor...'}/>

                <ScrollView>
                    <View style={{...margin('16 16 0 16')}}>
                        <View style={styles.card}>
                            {this.renderForm()}
                        </View>
                        <MpButton
                            title={t('Grafiği Göster')}
                            loading={loading}
                            containerStyle={styles.gotoButton}
                            onPress={this.gotoReportViewer}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

export default withNavigation(ReportFormBuilderComponent);

const styles = StyleSheet.create({
    card: {
        minHeight: 228,
        backgroundColor: white,
        ...cardShade1,
        ...padding('16 16 40 16'),
    },
    gotoButton: {
        ...margin('20 56 0 56'),
    },
    titleContainer: {
        ...margin('0 20 28 20')
    },
    title: {
        marginBottom: 10,
        fontFamily: 'Roboto-Bold',
        fontSize: 20,
        letterSpacing: 0.5,
        color: white
    },
    titleLine: {
        width: 40,
        borderWidth: 2,
        borderColor: white
    }
});
