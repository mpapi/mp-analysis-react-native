import React from 'react';
import {View} from 'react-native';

import MpHeader from '../../../components/header';
import {MpLoader} from '../../../components/loader';
import {DataService} from '../../../services/data.service';
import baseStyles from '../../../styles';

export default class LoadingComponent extends React.Component<any> {

    dataService: DataService;

    constructor(props: any) {
        super(props);

        this.dataService = new DataService();

        this.gotoList();
    }

    getCategoryId = async () => {
        await this.dataService.load();
        return this.dataService.get('categoryId');
    };

    gotoList = async () => {
        const categoryId = await this.getCategoryId();

        this.props.navigation.navigate('ReportStack', {categoryId});
    };

    // Render any loader content that you like here
    render() {
        return (
            <View style={baseStyles.container}>
                <MpHeader title='Yükleniyor...'/>
                <View style={[baseStyles.container, baseStyles.center]}>
                    <MpLoader/>
                </View>
            </View>
        );
    }
}
