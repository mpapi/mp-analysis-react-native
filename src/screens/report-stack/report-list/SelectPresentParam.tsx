import _ from 'lodash';
import React, {Component} from 'react';

import MpSearchOverlay from '../../../components/search-overlay';
import {ProcessEntity} from '../../../entity/ProcessEntity';
import {SelectEntity} from '../../../entity/SelectEntity';

export interface IProps {
    processItem: ProcessEntity | undefined;
    onSelect: (presentParamKey: string) => void;
    onClose: () => void;
}

interface IState {
    loading: boolean;
    isVisible: boolean;
    data: SelectEntity[];
}

export default class SelectPresentParam extends Component<IProps, IState> {

    state = {
        loading: false,
        isVisible: false,
        data: [],
    };

    componentDidMount() {
        this.load();
    }

    shouldComponentUpdate(nextProps: Readonly<IProps>, nextState: Readonly<IState>, nextContext: any): boolean {
        const isChangeProcessItem = !_.isEqual(nextProps.processItem, this.props.processItem);
        const isChangeState = !_.isEqual(nextState, this.state);

        return isChangeProcessItem || isChangeState;
    }

    componentDidUpdate(prevProps: Readonly<IProps>, prevState: Readonly<IState>, snapshot?: any): void {
        const isChangeProcessItem = !_.isEqual(prevProps.processItem, this.props.processItem);

        if (isChangeProcessItem) {
            this.load();
        }
    }

    load = async () => {
        const {processItem} = this.props;

        if (_.isUndefined(processItem)) {
            return;
        }

        this.setState({loading: true, isVisible: true});

        const data: SelectEntity[] = _.map<any, SelectEntity>(processItem.presentParams, (item) => {
            return {
                text: item.title,
                value: item.key
            };
        });

        this.setState({
            loading: false,
            data: [...data, {text: 'Forma Git', value: 'goto-form-builder'}]
        });
    };

    onSelect = (presentParamKey: string) => {
        const {onSelect} = this.props;

        this.setState({isVisible: false});

        onSelect(presentParamKey);
    };

    onClose = () => {
        const {onClose} = this.props;

        this.setState({isVisible: false});

        onClose();
    };

    render() {
        const {data, loading, isVisible} = this.state;

        return (
            <MpSearchOverlay
                loading={loading}
                isVisible={isVisible}
                search={false}
                onChangeValue={this.onSelect}
                onBackdropPress={this.onClose}
                data={data}
            />
        );
    }
}
