import _ from 'lodash';
import React, {PureComponent} from 'react';
import {Alert, TouchableOpacity, View} from 'react-native';
import {NavigationEvents} from 'react-navigation';

import MpErrorText from '../../../components/error-text';
import {MpFlatList} from '../../../components/flat-list';
import {MpListItem} from '../../../components/flat-list/MpListItem';
import MpHeader from '../../../components/header';
import MpIcon from '../../../components/icon/MpIcon';
import {IScreenContextProps} from '../../../contexts/withScreenContext';
import {MpPresentParamEntity} from '../../../entity/MpPresentParamEntity';
import {ProcessEntity} from '../../../entity/ProcessEntity';
import {QueryEntity} from '../../../entity/QueryEntity';
import {ProcessService} from '../../../services/api/process.service';
import {DataService} from '../../../services/data.service';
import baseStyles, {listItemIconContainerStyle} from '../../../styles';

import SelectPresentParam from './SelectPresentParam';

export interface IProps extends IScreenContextProps {
}

interface IState {
    loading: boolean;
    errorText: string;
    tmpProcessList: ProcessEntity[];
    processList: ProcessEntity[];
    selectedProcessItem: ProcessEntity | undefined;
}

export default class ReportListComponent extends PureComponent<IProps, IState> {

    dataService: DataService;
    processService: ProcessService;

    state = {
        loading: false,
        errorText: '',
        tmpProcessList: [],
        processList: [],
        selectedProcessItem: undefined
    };

    constructor(props: any) {
        super(props);

        this.processService = new ProcessService(props.navigation);
        this.dataService = new DataService();
    }

    didFocus = () => {
        this.load();
    };

    getCategoryId = async () => {
        await this.dataService.load();
        return this.dataService.get('categoryId');
    };

    load = async () => {
        if (this.state.loading) {
            return;
        }

        this.setState({loading: true});

        try {
            const categoryId = await this.getCategoryId();

            const processList = await this.processService.list({
                queryItems: true,
                categoryId
            });

            const tmpProcessList = categoryId
                ? _.filter(processList, (item) => item.categoryId === categoryId)
                : [...processList];

            this.setState({tmpProcessList, processList, loading: false});

        } catch (e) {

            this.setState({errorText: e.message || 'Liste alınamadı!', loading: false});
        }

    };

    onPressItem = (processItem: ProcessEntity) => () => {

        const presentParam = _.find(processItem.presentParams, {default: true});

        this.checkParams(processItem, presentParam);
    };

    onPresentParamSelect = (presentParamKey: string) => {

        const processItem: any = this.state.selectedProcessItem;

        if (_.isUndefined(processItem)) {
            return;
        } else {
            this.setState({selectedProcessItem: undefined});

            const presentParam = _.find(processItem.presentParams, {key: presentParamKey});

            this.checkParams(processItem, presentParam);
        }
    };

    checkParams = (processItem: ProcessEntity, presentParam?: MpPresentParamEntity) => {

        const queryItem: QueryEntity | undefined = _.find(processItem.queryItems, {queryKey: 'main'});

        if (!queryItem) {
            Alert.alert('Hata!', 'Main Sorgusu Bulunamadı!');
            return;
        }

        const paramNames: string[] = _.map<any, string>(queryItem.params, (p) => p.name);
        queryItem.params = this.processService.prepareParams(queryItem.params || []);

        const values = this.processService.prepareValues(presentParam && presentParam.params) || {};

        for (const paramName of paramNames) {

            const value = _.get(values, paramName);

            if (_.isUndefined(value) || (_.isString(value) && _.isEmpty(value))) {
                return this.gotoFormBuilder(processItem, values);
            }
        }

        this.gotoReportViewer(processItem, values);
    };

    gotoFormBuilder = (processItem: ProcessEntity, values: any) => {

        const {navigate} = this.props.navigation;
        navigate('ReportFormBuilder', {processItem, values});
    };

    gotoReportViewer = (processItem: ProcessEntity, values: any) => {

        const {navigate} = this.props.navigation;
        navigate('ReportViewer', {processItem, values});
    };

    setSelected = (selectedProcessItem?: ProcessEntity) => () => {
        this.setState({selectedProcessItem});
    };

    renderListItem = ({item, index}: { item: ProcessEntity, index: number }) => {

        const itemProps: any = {
            leftElement: item.icon
                ? (
                    <MpIcon
                        containerStyle={listItemIconContainerStyle}
                        name={(item.icon || '').replace('mp-', '')}
                        size={18}
                    />
                )
                : (
                    <View style={listItemIconContainerStyle}/>
                ),
            rightElement: (
                <TouchableOpacity
                    activeOpacity={.8}
                    onPress={this.setSelected(item)}
                >
                    <MpIcon
                        containerStyle={{
                            ...listItemIconContainerStyle,
                            backgroundColor: 'transparent'
                        }}
                        name='more_vert'
                        size={18}
                    />
                </TouchableOpacity>
            ),
            title: item.title,
            subtitle: item.subtitle || item.title,
            index,
            onPress: this.onPressItem(item),
        };

        return (<MpListItem {...itemProps}/>);
    };

    render() {
        const {processList, loading, selectedProcessItem} = this.state;
        return (
            <View style={baseStyles.container}>
                <NavigationEvents onDidFocus={this.didFocus}/>
                <MpHeader title='Raporlar' subtitle={loading && 'Yükleniyor...'}/>
                <MpErrorText text={!loading && processList.length === 0 && 'Bu kategoriye ait rapor bulunamadı.'}/>

                <MpFlatList
                    data={processList}
                    renderItem={this.renderListItem}
                    refreshing={loading}
                    onRefresh={this.load}
                />
                <SelectPresentParam
                    processItem={selectedProcessItem}
                    onSelect={this.onPresentParamSelect}
                    onClose={this.setSelected(undefined)}
                />
            </View>
        );
    }
}
