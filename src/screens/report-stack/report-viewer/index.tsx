import _ from 'lodash';
import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {NavigationEvents} from 'react-navigation';

import MpErrorText from '../../../components/error-text';
import MpHeader from '../../../components/header';
import {MpScrollView} from '../../../components/scroll-view';
import {IScreenContextProps} from '../../../contexts/withScreenContext';
import {MpChartEntity} from '../../../entity/MpChartEntity';
import {ProcessEntity} from '../../../entity/ProcessEntity';
import {QueryEntity} from '../../../entity/QueryEntity';
import {ProcessService} from '../../../services/api/process.service';
import baseStyles from '../../../styles';
import {wait} from '../../../utils/wait';

import CarouselSlider from './CarouselSlider';
import ChartContainer from './ChartContainer';
import ChartLists from './ChartLists';

export interface IProps extends IScreenContextProps {
    navigation: any;
}

interface IState {
    loadingText: string | boolean;
    errorText: string | boolean;
    chartEntity: MpChartEntity | undefined;
    values: { [key: string]: any };
    queryItem?: QueryEntity;
    processItem: ProcessEntity;
    data: any[];

    slideIndex: number;
}

export default class ReportViewerComponent extends Component<IProps, IState> {

    processService: ProcessService;

    scrollRef: any;

    state = {
        loadingText: 'Parametreler Alınıyor...',
        errorText: false,
        values: {},
        queryItem: undefined,
        processItem: new ProcessEntity(),
        chartEntity: undefined,
        data: [],

        slideIndex: 0,
    };

    constructor(props: any) {
        super(props);

        this.processService = new ProcessService(props.navigation);
    }

    didFocus = () => {
        this.load();
    };

    load = async () => {
        const {getParam} = this.props.navigation;

        const processItem: ProcessEntity | undefined = getParam('processItem');

        if (!processItem) {
            return this.setState({loadingText: false, errorText: 'İşlem Bulunamadı!'});
        }

        const chartEntity = this.getChartEntity(processItem);

        if (!chartEntity) {
            return this.setState({
                loadingText: false,
                errorText: 'Grafik ayarları bulunamadı!',
            });
        }

        const queryItem: QueryEntity | undefined = _.find(processItem.queryItems, {queryKey: 'main'});
        const values = getParam('values') || {};

        if (!queryItem) {
            return this.setState({
                loadingText: false,
                errorText: 'Main Query Bulunamadı!',
            });
        }

        this.setState({
            processItem,
            values,
            chartEntity,
            queryItem,
        }, () => {

            this.loadData();
        });
    };

    loadData = async () => {
        this.setState({loadingText: 'Veriler Alınıyor...'});
        const {values, processItem} = this.state;

        try {

            // @ts-ignore
            const {processKey} = processItem;

            const data = await this.processService.run(processKey, [{values}]);
            // TODO datayı kontrol etmek lazım. uygulama patlıyor...

            this.setState({data, loadingText: false, errorText: false});

        } catch (error) {

            console.log('errorText: ', error.message);
            this.setState({loadingText: false, errorText: error.message});
        }

    };

    onChangeChart = async (slideIndex: number) => {
        const {processItem} = this.state;

        this.setState({loadingText: 'Grafik Güncelleniyor...'}, async () => {

            await wait(800);

            const chartEntity = this.getChartEntity(processItem, slideIndex);

            this.setState({slideIndex, chartEntity, loadingText: false});
            this.scrollRef.scrollTo();
        });
    };

    getChartEntity = (processItem: ProcessEntity, slideIndex: number = 0): MpChartEntity | undefined => {
        return _.get(processItem, `charts[${slideIndex}]`);
    };

    shouldComponentUpdate(nextProps: Readonly<IProps>, nextState: Readonly<IState>, nextContext: any): boolean {
        return !_.isEqual(nextState, this.state);
    }

    render() {
        const {data, errorText, loadingText, chartEntity, processItem} = this.state;

        const title = _.get(processItem, 'title');
        const charts = _.get(processItem, 'charts');

        return (
            <View style={baseStyles.container}>
                <NavigationEvents onDidFocus={this.didFocus}/>
                <MpHeader title={title || 'Rapor Grafiği'} subtitle={loadingText}/>

                <MpErrorText text={errorText}/>

                <CarouselSlider
                    onChange={this.onChangeChart}
                    charts={charts}
                />

                <MpScrollView
                    ref={ref => this.scrollRef = ref}
                    refreshing={!!loadingText}
                    onRefresh={this.load}
                >

                    {
                        !loadingText && !errorText &&
                        <View
                            style={styles.scrollInside}
                        >

                            <ChartContainer
                                processItem={processItem}
                                chartEntity={chartEntity}
                                data={data}
                            />

                            <ChartLists
                                processItem={processItem}
                                chartEntity={chartEntity}
                                data={data}
                            />
                        </View>
                    }
                </MpScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    scrollInside: {
        flex: 1,
        paddingBottom: 30
    },
});
