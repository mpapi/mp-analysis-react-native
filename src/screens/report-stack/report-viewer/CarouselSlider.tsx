import _ from 'lodash';
import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Card} from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';

import withScreenContext, {IScreenContextProps} from '../../../contexts/withScreenContext';
import {MpChartEntity} from '../../../entity/MpChartEntity';
import baseStyles, {margin, padding, white} from '../../../styles';
import {blueCard} from '../../../styles/shadow';

export interface IProps extends IScreenContextProps {
    charts: MpChartEntity[];
    onChange: any;
}

interface IState {
}

class CarouselSlider extends Component<IProps, IState> {

    renderCarouselItem = ({item}: { item: MpChartEntity, index: number }) => {

        return (
            <Card containerStyle={styles.slideCard}>
                <Text style={styles.slideCardTitle}>{item.title}</Text>
                <Text style={styles.slideCardSubtitle}>{item.subtitle}</Text>
                <View style={styles.slideCardLine}/>
            </Card>
        );
    };

    shouldComponentUpdate(nextProps: Readonly<IProps>, nextState: Readonly<IState>, nextContext: any): boolean {
        return !_.isEqual(nextProps, this.props);
    }

    render() {
        const {charts, onChange, width} = this.props;

        return (
            <View style={{height: 150}}>
                <Carousel
                    data={charts || []}
                    renderItem={this.renderCarouselItem}
                    sliderWidth={width}
                    itemWidth={width - 32}
                    firstItem={0}
                    onBeforeSnapToItem={onChange}
                    inactiveSlideScale={1}
                    inactiveSlideOpacity={.8}
                />
            </View>
        );
    }
}

export default withScreenContext(CarouselSlider);

const styles = StyleSheet.create({
    slideCard: {
        ...blueCard,
        ...margin('25 8'),
        ...padding('20'),
    },
    slideCardTitle: {
        ...baseStyles.title,
        fontSize: 18,
        color: white
    },
    slideCardSubtitle: {
        ...baseStyles.subtext,
        ...margin('9 0 2 0'),
        fontSize: 14,
        color: white
    },
    slideCardLine: {
        width: 54,
        height: 2,
        backgroundColor: white
    },
});
