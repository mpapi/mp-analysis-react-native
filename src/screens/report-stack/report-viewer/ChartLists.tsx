import _ from 'lodash';
import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Card} from 'react-native-elements';

import {MpFlatList} from '../../../components/flat-list';
import {MpListItem} from '../../../components/flat-list/MpListItem';
import withScreenContext, {IScreenContextProps} from '../../../contexts/withScreenContext';
import {MpChartEntity} from '../../../entity/MpChartEntity';
import {MpSeriesEntity} from '../../../entity/MpSeriesEntity';
import {ProcessEntity} from '../../../entity/ProcessEntity';
import baseStyles, {grey, margin, padding, white} from '../../../styles';
import {cardShade1, cardShade2} from '../../../styles/shadow';

export interface IProps extends IScreenContextProps {
    processItem: ProcessEntity;
    chartEntity: MpChartEntity;
    data: any[];
}

interface IState {
}

class ChartLists extends Component<IProps, IState> {

    renderListItem = (series: MpSeriesEntity) => ({item, index}: { item: ProcessEntity, index: number }) => {

        const {chartEntity} = this.props;

        const emptyText = '[Boş Veri]';
        const title = _.get(item, chartEntity.titleColumnName);
        const subtitle = _.get(item, chartEntity.subtitleColumnName);

        const text = _.get(item, series.columnName);

        return (
            <MpListItem
                rightElement={
                    <Text>
                        {text}
                    </Text>
                }
                title={title || emptyText}
                subtitle={subtitle || emptyText}
                index={index}
            />
        );
    };

    shouldComponentUpdate(nextProps: Readonly<IProps>, nextState: Readonly<IState>, nextContext: any): boolean {
        return !_.isEqual(nextProps, this.props);
    }

    isRender = () => {
        const {chartEntity, data} = this.props;

        if (!data || data.length === 0) {
            return false;
        }

        if (!chartEntity) {
            return false;
        }

        const seriesEmpty = !chartEntity.series || chartEntity.series.length === 0;
        const titleEmpty = _.isEmpty(chartEntity.titleColumnName);

        return !seriesEmpty && !titleEmpty;
    };

    render() {
        const {chartEntity, data} = this.props;

        if (!this.isRender()) {
            return null;
        }

        return (
            <View>
                <View style={styles.lists}>
                    {
                        _.map(chartEntity.series, (series: MpSeriesEntity, index: number) => (
                            <View
                                style={styles.listGroup}
                                key={`values-list-${index}`}
                            >
                                <View>
                                    <Text style={styles.listTitle}>
                                        {series.text}
                                    </Text>
                                </View>
                                <Card containerStyle={styles.listCard}>
                                    <MpFlatList
                                        data={data}
                                        renderItem={this.renderListItem(series)}
                                    />
                                </Card>
                            </View>
                        ))
                    }
                </View>
            </View>
        );
    }
}

export default withScreenContext(ChartLists);

const styles = StyleSheet.create({
    card: {
        ...cardShade2,
        ...padding('16 16 40 16'),
        ...margin('10 16 20 16'),
        minHeight: 228,
    },
    titleContainer: {
        ...margin('0 20 28 20')
    },
    title: {
        marginBottom: 10,
        fontFamily: 'Roboto-Bold',
        fontSize: 20,
        letterSpacing: 0.5,
        color: white
    },
    titleLine: {
        width: 40,
        borderWidth: 2,
        borderColor: white
    },
    lists: {
        marginTop: 10
    },
    listGroup: {
        ...margin('12 16')
    },
    listTitle: {
        ...margin('8 18'),
        ...baseStyles.title,
        fontSize: 13,
        color: grey,
    },
    listCard: {
        ...cardShade1
    },
});
