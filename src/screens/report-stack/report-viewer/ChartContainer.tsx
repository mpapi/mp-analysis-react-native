import _ from 'lodash';
import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {Card} from 'react-native-elements';

import MpChart, {chartPadding} from '../../../components/chart/MpChart';
import withScreenContext, {IScreenContextProps} from '../../../contexts/withScreenContext';
import {MpChartEntity} from '../../../entity/MpChartEntity';
import {ProcessEntity} from '../../../entity/ProcessEntity';
import {margin, padding} from '../../../styles';
import {cardShade2} from '../../../styles/shadow';

export interface IProps extends IScreenContextProps {
    processItem: ProcessEntity;
    chartEntity: MpChartEntity;
    data: any[];
}

interface IState {
}

class ChartContainer extends Component<IProps, IState> {

    shouldComponentUpdate(nextProps: Readonly<IProps>, nextState: Readonly<IState>, nextContext: any): boolean {
        return !_.isEqual(nextProps, this.props);
    }

    render() {
        const {width, chartEntity, data} = this.props;

        const chartProps = {
            containerSize: {
                width: width - 48 - chartPadding.left - chartPadding.right,
                height: 170 + chartPadding.top + chartPadding.bottom
            },
            chartEntity,
            data
        };

        return (
            <Card containerStyle={{
                ...styles.card
            }}>
                {
                    <MpChart
                        {...chartProps}
                    />
                }
            </Card>
        );
    }
}

export default withScreenContext(ChartContainer);

const styles = StyleSheet.create({
    card: {
        ...cardShade2,
        ...padding('16'),
        ...margin('10 16 10 16'),
        minHeight: 228,
    },
});
