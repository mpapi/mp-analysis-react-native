import React from 'react';
import {Alert, StyleSheet, View} from 'react-native';
import {Text} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import {NavigationEvents} from 'react-navigation';

import MpButton from '../../../components/button/MpButton';
import MpEmailInput from '../../../components/form-elements/MpEmailInput';
import MpPasswordInput from '../../../components/form-elements/MpPasswordInput';
import KeyboardShift from '../../../components/KeyboardShift/KeyboardShift';
import {UserService} from '../../../services/api/user.service';
import {NavigationService} from '../../../services/navigation.service';
import {SessionService} from '../../../services/session.service';
import baseStyles, {margin, padding, primary, white} from '../../../styles';
import {cardShade2} from '../../../styles/shadow';

export interface IProps {
    navigation: any;
    t: any;
}

interface IState {
    loading: boolean;
    email: string;
    password: string;
}

export default class Index extends React.Component<IProps, IState> {

    state: IState = {
        loading: false,
        email: '',
        password: ''
    };

    sessionService: SessionService;
    userService: UserService;
    navigationService: NavigationService;

    constructor(props: IProps) {
        super(props);

        this.sessionService = SessionService.getInstance();
        this.userService = new UserService(props.navigation);
        this.navigationService = new NavigationService(props.navigation);
    }

    didFocus = async () => {
        if (this.sessionService.hasToken()) {
            const user = await this.sessionService.getUser();

            if (user) {
                this.setState({email: user.email});
            }
        }

        await this.onAuth();
    };

    gotoDrawerNavigation = () => {
        this.navigationService.resetNavigate('drawerStack');
    };

    onChangeText = (field: string) => (text: string) => {
        this.setState((state) => ({...state, [field]: text}));
    };

    onAuth = async () => {
        const hasToken = await this.sessionService.hasToken();

        const {loading} = this.state;

        if (loading || !hasToken) {
            console.log('skip auth');
            return;
        }

        this.setState({loading: true});

        try {
            await this.userService.auth();

            return this.gotoDrawerNavigation();
        } catch (e) {

            // skip navigation
            console.log('/user/auth catch: ', e.message);
        }

        this.setState({loading: false});
    };

    onLogin = async () => {
        const {loading, email, password} = this.state;

        if (loading) {
            return;
        }

        this.setState({loading: true});

        try {

            await this.userService.login({email, password});

            return this.gotoDrawerNavigation();
        } catch (e) {

            Alert.alert('Hata!', e.message);
        }

        this.setState({loading: false});
    };

    render() {
        const {loading, email, password} = this.state;
        const {t} = this.props;

        return (
            <KeyboardShift>
                <NavigationEvents onDidFocus={this.didFocus}/>
                <LinearGradient
                    locations={[0.7, 0.7]}
                    colors={[primary, white]}
                    style={[styles.gradientContainer]}>
                    <View style={styles.topEmpty}/>

                    <View style={styles.cardContainer}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.title}>
                                {t('Giriş')}
                            </Text>
                            <View style={styles.titleLine}/>
                        </View>

                        <View style={styles.card}>
                            <MpEmailInput
                                label={t('Email')}
                                value={email}
                                onChangeText={this.onChangeText('email')}
                            />

                            <MpPasswordInput
                                label={t('Parola')}
                                value={password}
                                onChangeText={this.onChangeText('password')}
                            />
                        </View>

                        <MpButton
                            containerStyle={styles.loginButton}
                            title={t('Giriş')}
                            loading={loading}
                            onPress={this.onLogin}/>
                    </View>

                    <View style={styles.bottomEmpty}/>
                </LinearGradient>
                {/*<View style={baseStyles.container}>
                    <View style={styles.blueContainer}/>
                    <View style={styles.whiteContainer}>
                        <View style={styles.cardContainer}>
                            <View style={styles.titleContainer}>
                                <Text style={styles.title}>
                                    {t('Giriş')}
                                </Text>
                                <View style={styles.titleLine}/>
                            </View>

                            <View style={styles.card}>
                                <View>
                                    <MpPasswordInput
                                        label={t('Parola')}
                                        value={password}
                                        onChangeText={this.onChangeText('password')}
                                    />

                                    <MpEmailInput
                                        label={t('Email')}
                                        value={email}
                                        onChangeText={this.onChangeText('email')}
                                    />
                                </View>
                            </View>

                            <MpButton
                                containerStyle={styles.loginButton}
                                title={t('Giriş')}
                                loading={loading}
                                onPress={this.onLogin}/>
                        </View>
                    </View>
                </View>*/}
            </KeyboardShift>
        );
    }
}

const styles = StyleSheet.create({
    blueContainer: {
        flex: 6,
        backgroundColor: primary
    },
    whiteContainer: {
        flex: 4,
        backgroundColor: white
    },
    gradientContainer: {
        ...baseStyles.container
    },
    topEmpty: {
        flex: .9
    },
    cardContainer: {
        ...margin('16'),
    },
    bottomEmpty: {
        flex: .1
    },
    card: {
        ...cardShade2,
        ...padding('16 16 12 16'),
    },
    loginButton: {
        ...margin('20 56 0 56')
    },
    titleContainer: {
        ...margin('0 20 28 20')
    },
    title: {
        marginBottom: 10,
        fontSize: 20,
        ...baseStyles.title,
        color: white
    },
    titleLine: {
        width: 40,
        borderWidth: 2,
        borderColor: white
    }
});
