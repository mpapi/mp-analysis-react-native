import React, {Component} from 'react';
import {Alert, View} from 'react-native';
import {NavigationEvents} from 'react-navigation';

import {MpFlatList} from '../../../components/flat-list';
import {MpListItem} from '../../../components/flat-list/MpListItem';
import MpHeader from '../../../components/header';
import {IScreenContextProps} from '../../../contexts/withScreenContext';
import {TaskEntity} from '../../../entity/TaskEntity';
import {TaskService} from '../../../services/api/task.service';
import baseStyles from '../../../styles';

export interface IProps extends IScreenContextProps {
}

interface IState {
    loadingText: string | boolean;
    tasks: Array<TaskEntity<any>>;
}

export default class TaskListComponent extends Component<IProps, IState> {

    taskService: TaskService;

    state = {
        loadingText: false,
        tasks: []
    };

    constructor(props: any) {
        super(props);

        this.taskService = new TaskService(props.navigation);
    }

    didFocus = () => {

        this.load();
    };

    load = async () => {
        const {loadingText} = this.state;

        if (loadingText) {
            return;
        }

        this.setState({loadingText: 'Yükleniyor...'});

        try {

            const tasks = await this.taskService.list();

            return this.setState({loadingText: false, tasks});
        } catch (e) {

            Alert.alert('Hata!', 'Task List: ' + e.message);
        }
    };

    gotoDetails = (taskKey?: string) => () => {
        const {navigate} = this.props.navigation;

        navigate('TaskDetails', {taskKey});
    };

    renderItem = ({item, index}: { item: TaskEntity<any>, index: number }) => {
        const {title, key, module} = item;

        return (
            <MpListItem
                title={title}
                subtitle={`Module: ${module}`}
                index={index}
                chevron
                onPress={this.gotoDetails(key)}
            />
        );
    };

    render() {

        const {loadingText, tasks} = this.state;

        return (
            <View style={baseStyles.container}>
                <NavigationEvents onDidFocus={this.didFocus}/>

                <MpHeader
                    title='Görev Listesi'
                    subtitle={loadingText}
                />

                <MpFlatList
                    data={tasks}
                    renderItem={this.renderItem}
                    refreshing={!!loadingText}
                    onRefresh={this.load}
                />
            </View>
        );
    }
}
