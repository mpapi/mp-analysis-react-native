import React, {Component} from 'react';
import {Alert, ScrollView, TouchableOpacity, View} from 'react-native';
import {Text} from 'react-native-elements';
import {NavigationEvents} from 'react-navigation';

import {MpFlatList} from '../../../components/flat-list';
import {MpListItem} from '../../../components/flat-list/MpListItem';
import MpHeader from '../../../components/header';
import MpIcon from '../../../components/icon/MpIcon';
import {MpLoader} from '../../../components/loader';
import MpOverlay from '../../../components/overlay';
import {IScreenContextProps} from '../../../contexts/withScreenContext';
import {TaskService} from '../../../services/api/task.service';
import baseStyles, {headerButtonStyle, padding} from '../../../styles';
import {confirm} from '../../../utils/confirm';

export interface IProps extends IScreenContextProps {
}

interface IState {
    listLoadingText: string | boolean;
    running: boolean;
    selected: string | null;
    logs: string[];
}

export default class TaskDetailsComponent extends Component<IProps, IState> {

    taskService: TaskService;

    state = {
        listLoadingText: false,
        running: false,
        selected: null,
        logs: []
    };

    constructor(props: any) {
        super(props);

        this.taskService = new TaskService(props.navigation);
    }

    didFocus = () => {

        this.load();
    };

    load = async () => {
        const {listLoadingText} = this.state;

        if (listLoadingText) {
            return;
        }

        this.setState({listLoadingText: 'Loglar yükleniyor...'});

        try {
            const {getParam} = this.props.navigation;
            const taskKey = getParam('taskKey');

            const logs = await this.taskService.logs(taskKey);

            return this.setState({listLoadingText: false, logs});
        } catch (e) {

            Alert.alert('Hata!', 'Task Details: ' + e.message);
        }
    };

    onOpenOverlay = (selected: string) => () => {
        this.setState({selected});
    };

    onRunTask = async () => {
        const result = await confirm('Uyarı!', 'Görev çalıştırılsın mı?');

        if (!result) {
            return;
        }

        this.setState({running: true});

        try {
            const {getParam} = this.props.navigation;
            const taskKey = getParam('taskKey');

            await this.taskService.run(taskKey);

        } catch (e) {
            Alert.alert('Hata!', 'Task Details: ' + e.message);
        }

        this.setState({running: false});
    };

    onCloseOverlay = () => {
        this.setState({selected: null});
    };

    renderItem = ({item, index}: { item: string, index: number }) => {
        return (
            <MpListItem
                title={item}
                index={index}
                onPress={this.onOpenOverlay(item)}
            />
        );
    };

    render() {

        const {listLoadingText, running, logs, selected} = this.state;

        return (
            <View style={baseStyles.container}>
                <NavigationEvents onDidFocus={this.didFocus}/>
                <MpHeader
                    title='Görev Detayları'
                    subtitle={listLoadingText}
                    rightComponent={
                        running
                            ? <MpLoader/>
                            : <TouchableOpacity
                                activeOpacity={1}
                                style={headerButtonStyle}
                                onPress={this.onRunTask}
                            >
                                <MpIcon
                                    name='play1'
                                />
                            </TouchableOpacity>
                    }
                />

                <MpFlatList
                    data={logs}
                    renderItem={this.renderItem}
                    refreshing={!!listLoadingText}
                    onRefresh={this.load}
                />

                <MpOverlay
                    isVisible={!!selected}
                    onBackdropPress={this.onCloseOverlay}
                >
                    <ScrollView style={{
                        ...baseStyles.container,
                        ...padding('8'),
                    }}>
                        <Text>
                            {selected}
                        </Text>
                    </ScrollView>
                </MpOverlay>
            </View>
        );
    }
}
