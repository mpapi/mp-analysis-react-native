import i18n from 'i18next';
import React from 'react';
import {initReactI18next} from 'react-i18next';
import {YellowBox} from 'react-native';

import {TR} from './locales';
import AppNavigator from './navigations';
import './utils/registers';

// locales imports
YellowBox.ignoreWarnings(['Remote debugger']);

i18n
    .use(initReactI18next)
    .init({
        resources: {
            tr: {translation: TR}
        },
        lng: 'tr',
        fallbackLng: 'tr',
        interpolation: {escapeValue: false}
    });

export default AppNavigator;
