import React from 'react';
import {getI18n, Translation} from 'react-i18next';
import {Dimensions} from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export interface IScreenContextProps {
    navigation: any;
    setLanguage: any;
    width: number;
    height: number;
    isSmallDevice: boolean;
    t: any;
}

const withScreenContext = <P extends object>(WrappedComponent: React.ComponentType<P>) => (props: P & any) => {

    const setLanguage = (lang: string) => {
        // set language
        const i18n = getI18n();
        i18n.changeLanguage(lang);
    };

    return (
        <Translation>
            {t => (
                <WrappedComponent
                    {...props}
                    {...{
                        t,
                        setLanguage,
                        width,
                        height,
                        isSmallDevice: width < 375,
                    }}
                />
            )}
        </Translation>
    );
};

export default withScreenContext;
