import {MpSeriesEntity} from './MpSeriesEntity';

export class MpChartEntity {
    key?: string;
    title!: string;
    subtitle!: string;
    icon?: string;
    default?: boolean;

    // x sütununun hangi veri alanından alacağını belirtir.
    isDate?: boolean;
    xColumnName!: string;

    titleColumnName!: string;
    subtitleColumnName!: string;

    // bir grafik içerisine birden fazla farklı şekiller eklenebilir.
    series!: MpSeriesEntity[];
}
