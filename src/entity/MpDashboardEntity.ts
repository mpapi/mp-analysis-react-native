export class MpDashboardEntity {
    processKey?: string;
    presentParamKey?: string;
    chartKey?: string;
    x!: number;
    y!: number;
    rows?: number;
    cols?: number;
}
