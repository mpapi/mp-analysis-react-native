export class KeyValueEntity<T, P> {
    key?: T;
    value?: P;
}
