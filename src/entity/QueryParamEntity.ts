import {SelectEntity} from './SelectEntity';

export class QueryParamEntity {
    title!: string;
    type!: string;
    source?: string;
    name!: string;
    values?: SelectEntity[];
}
