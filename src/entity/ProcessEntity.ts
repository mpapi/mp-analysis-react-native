import {MpChartEntity} from './MpChartEntity';
import {MpPresentParamEntity} from './MpPresentParamEntity';
import {ProcessEventEntity} from './ProcessEventEntity';
import {QueryEntity} from './QueryEntity';

export class ProcessEntity {
    _id?: string;
    categoryId?: string;
    categoryTitle?: string;
    title?: string;
    processKey!: string;
    systemType?: string;
    expiredTime?: string;
    dbKey?: string;
    icon?: string;
    queryItems?: QueryEntity[];
    events?: ProcessEventEntity[];

    presentParams?: MpPresentParamEntity[];
    charts?: MpChartEntity[];
    // local prepared vars
    subtitle?: string;
}
