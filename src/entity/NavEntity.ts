import {CategoryEntity} from './CategoryEntity';

export interface INavEntity extends CategoryEntity {
    badge?: any;
    route?: string;
    divider?: {
        after?: boolean,
        before?: boolean
    };
}
