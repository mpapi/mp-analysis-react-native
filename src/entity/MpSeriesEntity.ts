export class MpSeriesEntity {
    type!: string;
    isDate!: boolean;
    columnName!: string;
    text!: string;
}
