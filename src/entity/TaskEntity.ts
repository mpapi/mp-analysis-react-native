import {RequestEntity} from './RequestEntity';

export class TaskEntity<T> {

    userId!: string;
    key!: string;
    title!: string;
    module!: string;
    data!: RequestEntity[];

    options!: T;
}
