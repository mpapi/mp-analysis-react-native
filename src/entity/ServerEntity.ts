import uuid from 'uuid/v1';

export class ServerEntity {
    key: string;
    title!: string;
    host!: string;
    port!: number;
    isDefault!: boolean;

    public constructor({key, title = '', host = '', port = 80, isDefault = false}: { key?: string, title?: string, host?: string, port?: number, isDefault?: boolean }) {
        this.key = key || uuid();
        this.title = title;
        this.host = host;
        this.port = port;
        this.isDefault = isDefault;
    }
}
