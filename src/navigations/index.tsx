import React from 'react';
import {Animated, Easing} from 'react-native';
import {createAppContainer, createDrawerNavigator, createStackNavigator, createSwitchNavigator} from 'react-navigation';

import MpDrawerMenu from '../components/drawer/MpDrawerMenu';
import withScreenContext from '../contexts/withScreenContext';
import {LoadingScreen} from '../screens';

import DashboardStack from './DashboardStack';
import LoginStack from './LoginStack';
import ProfileStack from './ProfileStack';
import ReportStack from './ReportStack';
import TaskStack from './TaskStack';

const noTransitionConfig = () => ({
    transitionSpec: {
        duration: 0,
        timing: Animated.timing,
        easing: Easing.step0
    }
});

export const DrawerNavigator = createDrawerNavigator({
    DashboardStack,
    ReportStack: createSwitchNavigator(
        {
            SwitchCategory: LoadingScreen,
            ReportStack
        },
        {
            initialRouteName: 'SwitchCategory',
        }
    ),
    ProfileStack,
    TaskStack
}, {
    initialRouteName: 'DashboardStack',
    contentComponent: withScreenContext(MpDrawerMenu)
});

const Containers = createStackNavigator({
    loginStack: {screen: LoginStack},
    drawerStack: {screen: DrawerNavigator}
}, {
    headerMode: 'none',
    initialRouteName: 'loginStack',
    transitionConfig: noTransitionConfig
});

export default createAppContainer(Containers);
