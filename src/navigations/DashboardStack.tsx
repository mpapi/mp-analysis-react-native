// Reports stack
import {createStackNavigator} from 'react-navigation';

import withScreenContext from '../contexts/withScreenContext';
import {DashboardScreen} from '../screens';

const DashboardStack = createStackNavigator({
    Dashboard: {
        screen: withScreenContext<any>(DashboardScreen),
        navigationOptions: {
            title: 'Dashboard'
        }
    }
}, {
    headerMode: 'none',
    initialRouteName: 'Dashboard'
});

export default DashboardStack;
