// Profile stack
import {createStackNavigator} from 'react-navigation';

import withScreenContext from '../contexts/withScreenContext';
import {AboutScreen, AccessPointChangeScreen, AccessPointListScreen, PasswordChangeScreen, ProfileScreen} from '../screens';

const ProfileStack = createStackNavigator({
    Profile: {
        screen: withScreenContext<any>(ProfileScreen)
    },
    AccessPointList: {
        screen: withScreenContext<any>(AccessPointListScreen)
    },
    AccessPointChange: {
        screen: withScreenContext<any>(AccessPointChangeScreen)
    },
    About: {
        screen: withScreenContext<any>(AboutScreen)
    },
    PasswordChange: {
        screen: withScreenContext<any>(PasswordChangeScreen)
    }
}, {
    headerMode: 'none',
    initialRouteName: 'Profile'
});

export default ProfileStack;
