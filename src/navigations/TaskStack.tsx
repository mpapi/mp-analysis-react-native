// Reports stack
import {createStackNavigator} from 'react-navigation';

import withScreenContext from '../contexts/withScreenContext';
import {TaskDetailsScreen, TaskListScreen} from '../screens';

// Tasks stack
const TaskStack = createStackNavigator({
    TaskList: {
        screen: withScreenContext<any>(TaskListScreen)
    },
    TaskDetails: {
        screen: withScreenContext<any>(TaskDetailsScreen)
    },
}, {
    headerMode: 'none',
    initialRouteName: 'TaskList'
});

export default TaskStack;
