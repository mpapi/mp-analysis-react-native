// Reports stack
import React from 'react';
import {createStackNavigator} from 'react-navigation';

import withScreenContext from '../contexts/withScreenContext';
import {ReportFormBuilderScreen, ReportListScreen, ReportViewerScreen} from '../screens';

const ReportStack = createStackNavigator({
    ReportList: {
        screen: withScreenContext<any>(ReportListScreen),
        title: 'Rapor Listesi'
    },
    ReportFormBuilder: {
        screen: withScreenContext<any>(ReportFormBuilderScreen)
    },
    ReportViewer: {
        screen: withScreenContext<any>(ReportViewerScreen)
    },
}, {
    headerMode: 'none',
    initialRouteName: 'ReportList'
});

export default ReportStack;
