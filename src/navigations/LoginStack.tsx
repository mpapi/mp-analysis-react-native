// login stack
import {createStackNavigator} from 'react-navigation';

import withScreenContext from '../contexts/withScreenContext';
import {LoginScreen} from '../screens';

const LoginStack = createStackNavigator({
    loginScreen: {screen: withScreenContext<any>(LoginScreen)},
}, {
    headerMode: 'none'
});

export default LoginStack;
