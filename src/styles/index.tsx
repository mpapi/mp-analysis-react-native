import Color from 'color';
import {StyleSheet} from 'react-native';
import {getStatusBarHeight} from 'react-native-status-bar-height';

export const primary = '#78aef9';
export const primaryA1 = '#A2C8FC';
export const primaryA2 = '#DAE9FE';

export const accent = '#6AE1C4';
export const accentA1 = '#97F0DA';
export const accentA2 = '#CFFBF0';

export const black = '#33383D';
export const darkGrey = '#595F66';
export const grey = '#8D959D';
export const lightGrey = '#CED4DA';
export const paleGrey = '#F1F3F5';
export const white = '#FFF';
export const purple = '#C075C9';
export const blue = '#78AEF9';
export const aqua = '#65C6F0';
export const teal = '#50C1B6';
export const green = '#73CC72';
export const yellow = '#FFCC54';
export const orange = '#FF844D';
export const red = '#F65252';
export const pink = '#F45D90';

const index = StyleSheet.create({
    container: {
        flex: 1
    },
    statusBarSpace: {
        paddingTop: getStatusBarHeight()
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    vCenter: {
        justifyContent: 'center'
    },
    hCenter: {
        alignItems: 'center'
    },
    title: {
        fontFamily: 'Roboto-Bold',
        color: darkGrey,
        fontSize: 18
    },
    text: {
        fontFamily: 'Roboto-Regular',
        color: darkGrey,
        fontSize: 18
    },
    errorText: {
        color: red,
        fontSize: 16,
        fontFamily: 'Roboto-Regular',
    },
    subtext: {
        fontFamily: 'Roboto-Regular',
        color: grey,
        fontSize: 14,
    },
});

export default index;

export const lighten = (color: string, ratio: number): string => {
    return Color(color).lighten(ratio).hex();
};

export const darken = (color: string, ratio: number): string => {
    return Color(color).darken(ratio).hex();
};

export const margin = (str: string | number) => {

    const numbers = str.toString().split(' ');

    if (numbers.length === 1) {
        return {
            marginTop: parseInt(numbers[0], 10),
            marginRight: parseInt(numbers[0], 10),
            marginBottom: parseInt(numbers[0], 10),
            marginLeft: parseInt(numbers[0], 10)
        };
    } else if (numbers.length === 2) {
        return {
            marginTop: parseInt(numbers[0], 10),
            marginRight: parseInt(numbers[1], 10),
            marginBottom: parseInt(numbers[0], 10),
            marginLeft: parseInt(numbers[1], 10)
        };
    } else if (numbers.length === 4) {
        return {
            marginTop: parseInt(numbers[0], 10),
            marginRight: parseInt(numbers[1], 10),
            marginBottom: parseInt(numbers[2], 10),
            marginLeft: parseInt(numbers[3], 10)
        };
    }
};

export const padding = (str: string | number) => {

    const numbers = str.toString().split(' ');

    if (numbers.length === 1) {
        return {
            paddingTop: parseInt(numbers[0], 10),
            paddingRight: parseInt(numbers[0], 10),
            paddingBottom: parseInt(numbers[0], 10),
            paddingLeft: parseInt(numbers[0], 10)
        };
    } else if (numbers.length === 2) {
        return {
            paddingTop: parseInt(numbers[0], 10),
            paddingRight: parseInt(numbers[1], 10),
            paddingBottom: parseInt(numbers[0], 10),
            paddingLeft: parseInt(numbers[1], 10)
        };
    } else if (numbers.length === 4) {
        return {
            paddingTop: parseInt(numbers[0], 10),
            paddingRight: parseInt(numbers[1], 10),
            paddingBottom: parseInt(numbers[2], 10),
            paddingLeft: parseInt(numbers[3], 10)
        };
    }
};

export const radius = (str: string | number) => {

    const numbers = str.toString().split(' ');

    if (numbers.length === 1) {
        return {
            borderTopLeftRadius: parseInt(numbers[0], 10),
            borderTopRightRadius: parseInt(numbers[0], 10),
            borderBottomRightRadius: parseInt(numbers[0], 10),
            borderBottomLeftRadius: parseInt(numbers[0], 10),
        };
    } else if (numbers.length === 2) {
        return {
            borderTopLeftRadius: parseInt(numbers[0], 10),
            borderTopRightRadius: parseInt(numbers[1], 10),
            borderBottomRightRadius: parseInt(numbers[0], 10),
            bottomLeft: parseInt(numbers[1], 10)
        };
    } else if (numbers.length === 4) {
        return {
            borderTopLeftRadius: parseInt(numbers[0], 10),
            borderTopRightRadius: parseInt(numbers[1], 10),
            borderBottomRightRadius: parseInt(numbers[2], 10),
            borderBottomLeftRadius: parseInt(numbers[3], 10)
        };
    }
};

export const inputIconContainerStyle = {
    marginLeft: 0,
    marginRight: 8,
    width: 32,
    height: 32,
    ...index.center,
    borderRadius: 23,
    backgroundColor: lighten(lightGrey, .1)
};

export const inputValueContainerStyle = {
    backgroundColor: paleGrey,
    borderRadius: 4,
    borderBottomWidth: 0,
    height: 43
};

export const inputLabelStyle = {
    color: grey,
    fontFamily: 'Roboto-Bold',
    fontSize: 16,
    marginBottom: 9
};

export const inputValueStyle = {
    color: black,
    fontSize: 18,
    fontFamily: 'Roboto-Regular',
    ...margin('0 16')
};

export const inputErrorStyle = {
    ...index.errorText,
    ...margin('2 5')
};

export const inputContainerStyle = (isError: boolean) => ({
    marginBottom: isError ? 3 : 25
});

export const headerButtonStyle = {
    ...index.center,

    width: 46,
    height: 46,
};

export const listItemContainerStyle = {
    borderBottomWidth: 1,
    borderBottomColor: paleGrey,
    minHeight: 59
};

export const listItemIconContainerStyle = {
    marginLeft: 0,
    marginRight: 8,
    width: 46,
    height: 46,
    ...index.center,
    borderRadius: 23,
    backgroundColor: paleGrey,
};
