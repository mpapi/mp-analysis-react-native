import {Platform} from 'react-native';

import {black, margin, padding, paleGrey, primary, white} from './index';

const defaultCardStyles = {
    ...padding('8'),
    borderWidth: 0,
    minHeight: 60,
    backgroundColor: white,
};

export const shadow = (shade: number, shadowColor?: string) => {
    switch (shade) {
        case 2:
            return {
                ...Platform.select({
                    android: {
                        elevation: 6,
                    },
                    ios: {
                        shadowColor: shadowColor || black,
                        shadowOffset: {
                            width: 0,
                            height: 5,
                        },
                        shadowOpacity: 0.34,
                        shadowRadius: 6.27,

                    }
                }),
                borderRadius: 8,
            };
        case 1:
        default:
            return {
                ...Platform.select({
                    android: {
                        elevation: 2,
                    },
                    ios: {
                        shadowColor: shadowColor || black,
                        shadowOffset: {
                            width: 0,
                            height: 1,
                        },
                        shadowOpacity: 0.20,
                        shadowRadius: 1.41,
                    }
                }),
                borderRadius: 5,
            };
    }
};

export const cardShade1 = {
    ...shadow(1),
    ...margin('0'),
    ...defaultCardStyles,
};

export const cardShade2 = {
    ...shadow(2),
    ...margin('0'),
    ...defaultCardStyles,
};

export const blueCard = {
    ...shadow(2, primary),
    ...margin('25'),
    ...defaultCardStyles,
    backgroundColor: primary,
};
