import {registerCustomIconType} from 'react-native-elements';

import {MpFontIcon} from '../components/icon/MpFontIcon';

///////////////////////////////////
// font icon register
///////////////////////////////////
registerCustomIconType('MP-API', MpFontIcon);
