import {Alert} from 'react-native';

export const confirm = async (title: string, message: string) => {
    return new Promise<any>((resolve) => {

        Alert.alert(
            title,
            message,
            [
                {
                    text: 'İptal',
                    style: 'cancel',
                    onPress: () => resolve(false)
                },
                {
                    text: 'Evet',
                    onPress: () => resolve(true)
                },
            ],
            {
                cancelable: false,
                onDismiss: () => resolve(false)
            },
        );

    });
};
