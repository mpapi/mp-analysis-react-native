import _ from 'lodash';
import moment from 'moment';

import {ProcessEntity} from '../../entity/ProcessEntity';
import {QueryParamEntity} from '../../entity/QueryParamEntity';
import {ApiService} from '../api.service';

export class ProcessService {

    protected api: ApiService;

    constructor(protected navigation: any) {
        this.api = new ApiService(navigation);
    }

    async list(params?: any): Promise<ProcessEntity[]> {

        const res = await this.api.post('/process/list', {
            mpAnalysis: true,
            ...params
        });

        const items: ProcessEntity[] = res.items;

        for (const item of items) {

            if (!item.presentParams) {
                continue;
            }

            for (const presentParam of item.presentParams) {

                presentParam.title = this.textToDateString(presentParam.title || '');
                presentParam.subtitle = this.textToDateString(presentParam.subtitle || '');
                presentParam.description = this.textToDateString(presentParam.description || '');

                presentParam.params = this.prepareValues(presentParam.params);
            }
        }

        return items;
    }

    async get(params: any): Promise<ProcessEntity> {
        const res = await this.api.post('/process/get', params);
        return res.item;
    }

    async run(processKey: string, parameters: any) {
        const result = await this.api.clientPost('/process/run', {processKey, parameters});

        return result.items || [];
    }

    textToDateString(value: string): string {
        const matches = value.toString().match(/(\${[a-zA-Z0-9\-\|\:]+}|\${})/gm);

        let val = value;

        if (matches != null) {

            for (const match of  _.uniq<string>(matches)) {
                const date = this.toDate(match);
                val = val.replace(match, date);
            }
        }

        return val;
    }

    prepareValues(values: any): any {

        const keys = _.keys(values);

        for (const key of keys) {
            values[key] = this.textToDateString(values[key]);
        }

        return values;
    }

    prepareParams(params: QueryParamEntity[]): QueryParamEntity[] {

        for (const param of params) {

            param.title = this.textToDateString(param.title || '');

            for (const value of param.values || []) {
                value.text = this.textToDateString(value.text || '');
                value.value = this.textToDateString(value.value);
            }
        }

        return params;
    }

    private getDateFormat(dateParam: string) {
        const matches = dateParam.match(/format:([a-zA-Z\-]+)/);
        if (matches != null && matches.length > 0) {
            return matches[0].replace('format:', '');
        }
        return 'YYYY-MM-DD HH:mm:ss';
    }

    private toDate(match: string): string {

        const dateParams = match.replace(/(\$|\{|\})/gm, '')
            .split('||');

        const date = moment();

        const dateFormat = this.getDateFormat(match);

        for (const dateParam of dateParams) {

            const arg1 = dateParam.match(/(^.[0-9]+)/gim);
            const arg2 = dateParam.match(/([a-z]+)$/gim);

            if (arg1 != null && arg2 != null) {
                const duration = parseInt(arg1[0], 10);
                const type = arg2[0].toLowerCase();

                // @ts-ignore
                date.add(duration, type);
            }
        }

        return date.format(dateFormat);
    }

}
