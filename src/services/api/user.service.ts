import _ from 'lodash';
import {Alert} from 'react-native';

import {MpDashboardEntity} from '../../entity/MpDashboardEntity';
import {UserEntity} from '../../entity/UserEntity';
import {ApiService} from '../api.service';
import {SessionService} from '../session.service';

export class UserService {

    protected api: ApiService;
    protected sessionService: SessionService;

    constructor(protected navigation: any) {
        this.api = new ApiService(navigation);
        this.sessionService = SessionService.getInstance();
    }

    public async login(user: UserEntity, force?: boolean) {
        try {
            const result: any = await this.api.post('/user/login', {
                force: force === true,
                email: user.email,
                password: user.password
            });

            await this.sessionService.set(result);

            return result.user;
        } catch (error) {

            if (error.message === 'SESSION_IS_OPEN') {

                return new Promise<any>((resolve, reject) => {

                    Alert.alert(
                        'Dikkat!',
                        'Farklı yerde oturum açık. Oturum burada açılsın mı?',
                        [
                            {
                                text: 'İptal',
                                style: 'cancel',
                                onPress: () => reject(error)
                            },
                            {
                                text: 'Evet',
                                onPress: () => this.login(user, true).then(resolve, reject)
                            },
                        ],
                        {
                            cancelable: false,
                        },
                    );

                });

            } else {
                throw error;
            }
        }
    }

    async logout(): Promise<any> {
        const res = await this.api.post('/user/logout');

        // oturumu temizler
        await this.sessionService.clear();

        // login sayfasına yönlendirir.
        await this.api.gotoLoginStack();

        return res;
    }

    async auth(): Promise<any> {

        const res = await this.api.post('/user/auth');

        await this.sessionService.setUser({...res.user});
        return res;
    }

    async dashboardItems(): Promise<MpDashboardEntity[]> {
        const user = await this.sessionService.getUser();

        const userDashboard = user.dashboard || [];

        const group = _.groupBy(userDashboard, (item) => item.y);

        const rows: any = [];

        const keys = _.keys(group);
        for (const key of keys) {
            rows.push(group[key]);
        }

        return rows;
    }

}
