import _ from 'lodash';

import {ApiService} from '../api.service';

export class TaskService {

    protected api: ApiService;

    constructor(protected navigation: any) {
        this.api = new ApiService(navigation);
    }

    public async list() {

        const result = await this.api.post('/user/tasks');

        return result.items || [];
    }

    public async run(taskKey: string) {

        return this.api.clientPost('/task/run', {taskKey});
    }

    public async logs(taskKey: string) {
        const result = await this.api.clientPost('/task/logs', {taskKey});

        return _.reverse(result.items) || [];
    }

}
