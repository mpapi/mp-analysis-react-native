import {ApiService} from '../api.service';

export class DashboardService {

    protected api: ApiService;

    constructor(protected navigation: any) {
        this.api = new ApiService(navigation);
    }

    public async status() {

        return this.api.post('/mp-analysis/dashboard/status');
    }

}
