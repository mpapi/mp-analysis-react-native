import {ApiService} from '../api.service';

export class CategoryService {

    protected api: ApiService;

    constructor(protected navigation: any) {
        this.api = new ApiService(navigation);
    }

    async list(): Promise<any> {
        const res = await this.api.post('/category/list');

        return res.items;
    }

}
