import * as _ from 'lodash';

import {UserEntity} from '../entity/UserEntity';

import {DataService} from './data.service';

export class SessionService {

    private static instance: SessionService;

    private data: DataService;

    private user!: UserEntity;
    private token!: string;

    private constructor() {
        this.data = new DataService();
    }

    public static getInstance(): SessionService {
        if (!this.instance) {
            this.instance = new SessionService();
        }

        return this.instance;
    }

    public async set(result: { user: UserEntity, token: string }) {

        this.user = result['user'];

        this.token = result['token'];

        await this.save();
    }

    public async setUser(user: UserEntity) {

        this.user = user;

        await this.save();
    }

    public async clear() {

        await this.data
            .clear(['user', 'token'])
            .save();
    }

    public async getUser(): Promise<UserEntity> {

        await this.read();

        return this.user;
    }

    public async getToken(): Promise<string> {
        await this.read();
        return this.token;
    }

    public async hasToken() {
        await this.read();
        return !_.isUndefined(this.token);
    }

    public getUserDefaultValues() {
        const params: any = {};

        const defaults = (this.user && this.user.defaults) || [];

        for (const def of defaults) {
            params[def.name] = def.value;
        }

        return params;
    }

    private async read() {
        await this.data.load();

        this.user = await this.data.get<UserEntity>('user');
        this.token = await this.data.get<string>('token');
    }

    private async save() {
        console.log('Save -> Token: ', this.token, 'UserId: ', this.user._id);

        await this.data
            .set('user', this.user)
            .set('token', this.token)
            .save();
    }

    getFullnameOrUsername(): string {
        if (!this.user) {
            return 'MP';
        }

        return this.user.fullname || this.user.username || 'M P';
    }

    getAvatarText(): string {
        return this.getFullnameOrUsername().split(' ').map((l) => l.charAt(0)).join('').substring(0, 2);
    }
}
