import {green, orange, primary, purple, yellow} from '../styles';

export class ColorService {

    colors = [
        primary,
        orange,
        purple,
        yellow,
        green,
    ];

    constructor() {
    }

    public getColor(index: number) {
        return this.colors[index % this.colors.length];
    }
}
