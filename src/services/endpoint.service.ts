import * as _ from 'lodash';

import {ServerEntity} from '../entity/ServerEntity';

import {DataService} from './data.service';

export class EndpointService {

    private static instance: EndpointService;
    private readonly data: DataService;
    private servers: ServerEntity[] = [];

    private constructor() {
        this.data = new DataService();
    }

    public static getInstance(): EndpointService {
        if (!this.instance) {
            this.instance = new EndpointService();
        }

        return this.instance;
    }

    public async save(server: ServerEntity) {
        await this.read();

        if (server.isDefault) {
            _.forEach(this.servers, (s) => {
                s.isDefault = false;
            });
        }

        // Find item index using _.findIndex (thanks @AJ Richardson for comment)
        const index = _.findIndex(this.servers, {key: server.key});

        if (index >= 0) {
            // Replace item at index using native splice
            this.servers.splice(index, 1, server);

        } else {
            this.servers.push(server);
        }

        _.sortBy(this.servers, ['title']);

        await this.saveData();
    }

    public async delete(key: string) {
        await this.read();

        // Find item index using _.findIndex (thanks @AJ Richardson for comment)
        const index = _.findIndex(this.servers, {key});

        if (index >= 0) {

            // Replace item at index using native splice
            this.servers.splice(index, 1);
        }

        await this.saveData();
    }

    public has(key: string) {

        const index = _.findIndex(this.servers, {key});

        return index >= 0;
    }

    public async list(): Promise<ServerEntity[]> {

        await this.read();

        return this.servers;
    }

    public async getDefaultUrl() {
        await this.read();

        const server = _.find(this.servers, {isDefault: true});

        if (server) {
            return server.host + (server.port === 80 ? '' : ':' + server.port);
        } else {
            return 'http://client-server.hakanketen.com';
        }
    }

    public async clear() {
        await this.data.clear(['servers']);
    }

    private async read() {
        await this.data.load();
        this.servers = await this.data.get<ServerEntity[]>('servers');

        if (!this.servers) {
            this.servers = [];
        }
    }

    private async saveData() {
        await this.data
            .set('servers', this.servers)
            .save();
    }
}
