import {NavigationActions, StackActions} from 'react-navigation';

export class NavigationService {

    public constructor(protected navigation: any) {
    }

    resetNavigate = (routeName: string, params?: any) => {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName, params})],
        });

        return this.navigation.dispatch(resetAction);
    };

    navigate = (screen: string, params?: any) => {
        return this.navigation.navigate(screen, params);
    };

    back = () => {
        return this.navigation.goBack();
    };

}
