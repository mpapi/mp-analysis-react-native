import AsyncStorage from '@react-native-community/async-storage';
import * as _ from 'lodash';

export class DataService {

    private static data: any = {};

    private key = '@AppData';

    private isSaved = true;

    constructor() {
    }

    public set(name: string, value: any) {
        this.isSaved = false;

        DataService.data[name] = _.cloneDeep(value);

        return this;
    }

    public get<T>(name: string): T {
        return DataService.data[name];
    }

    public clear(names: string[]) {
        this.isSaved = false;

        for (const name of names) {

            delete DataService.data[name];
        }

        return this;
    }

    public async save() {
        await AsyncStorage.setItem(this.key, JSON.stringify(DataService.data));
        this.isSaved = true;
        return this;
    }

    public async load() {
        if (!this.isSaved) {
            throw new Error('Data is not saved');
        }

        const data = await AsyncStorage.getItem(this.key);

        DataService.data = data == null ? {} : JSON.parse(data);

        return this;
    }
}
