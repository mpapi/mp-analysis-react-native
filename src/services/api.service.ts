import {EndpointService} from './endpoint.service';
import {NavigationService} from './navigation.service';
import {SessionService} from './session.service';

export class ApiService {

    baseUrl = 'https://main-server.hakanketen.com';

    protected session: SessionService;
    protected endpoint: EndpointService;
    public ns: NavigationService;

    constructor(protected navigation: any) {
        this.session = SessionService.getInstance();
        this.endpoint = EndpointService.getInstance();
        this.ns = new NavigationService(navigation);
    }

    public gotoLoginStack = () => {
        this.ns.resetNavigate('loginStack');
    };

    private async parseJson(response: Response) {
        try {
            return response.json();
        } catch (e) {
            throw new Error('Alınan veri işlenemedi.');
        }

    }

    public request = async (method: string, url: string, opts?: any, headerParams?: any, limit: number = 3): Promise<any> => {

        opts = opts || {};
        opts.responseType = 'json';
        opts.method = method.toUpperCase();
        opts.headers = {
            ...(headerParams || {}),
            'Content-Type': 'application/json'
        };

        if (this.session.hasToken()) {
            opts.headers.token = await this.session.getToken();
        }

        console.log('request >>>>>: ', url, opts);

        const response = await fetch(url, opts);

        console.log('<<<<<<<<<<<<>>>>>>>>>>>>:', response);

        if (!response.ok) {
            if (limit >= 0) {
                return this.request(method, url, opts, headerParams, limit - 1);
            } else {
                throw new Error('İstek Tamamlanamadı!');
            }
        }

        const result = await this.parseJson(response);

        console.log('request <<<<<: ', url, result);

        if (result.error) {
            console.log('result.error:', result.error);
            if (this.navigation) {
                const {routeName} = this.navigation.state;

                if (this.isRoutingError(result.error)) {
                    await this.session.clear();

                    if (routeName !== 'loginScreen') {
                        this.gotoLoginStack();
                    }
                }
            }
            throw new Error(result.error);
        }

        return result;
    };

    public isRoutingError = (errorMessage: string) => {
        switch (errorMessage) {
            case 'SESSION_NOT_FOUND':
            case 'TOKEN_NOT_FOUND':
                return true;
            default:
                return false;
        }
    };

    public clientPost = async (path: string, params?: any, headerParams?: string[][2]) => {

        const clientUrl = await this.endpoint.getDefaultUrl();
        const defaults = this.session.getUserDefaultValues();

        return this.request('post', clientUrl + path, {
            body: JSON.stringify({
                ...params,
                defaults
            })
        }, headerParams);
    };

    public post = (path: string, params?: any, headerParams?: string[][2]) => {
        return this.request('post', this.baseUrl + path, {
            body: JSON.stringify(params || {})
        }, headerParams);
    };

}
