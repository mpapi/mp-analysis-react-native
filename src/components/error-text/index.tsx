import * as _ from 'lodash';
import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {Text} from 'react-native-elements';

import baseStyles, {lighten, padding, red} from '../../styles';

export interface IProps {
    containerStyle: any;
    textStyle: any;
    text: any;
}

class MpErrorText extends Component<IProps> {

    static defaultProps = {
        containerStyle: {},
        textStyle: {},
    };

    render() {
        const {containerStyle, textStyle, text, ...otherProps} = this.props;

        if (!_.isString(text)) {
            return null;
        }

        return (
            <View
                style={{
                    ...styles.container,
                    ...baseStyles.center,
                    ...containerStyle
                }}
                {...otherProps}
            >
                <Text style={{
                    ...baseStyles.errorText,
                    fontSize: 13,
                    ...textStyle
                }}>
                    {text}
                </Text>
            </View>
        );
    }
}

export default MpErrorText;

const styles = StyleSheet.create({
    container: {
        backgroundColor: lighten(red, .5),
        ...padding('10')
    },
});
