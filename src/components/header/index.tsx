import React, {Component} from 'react';
import {Platform, StyleSheet, View} from 'react-native';
import {Header, Text} from 'react-native-elements';
import {withNavigation} from 'react-navigation';

import baseStyles, {white} from '../../styles';

import MpBackButton from './MpBackButton';
import MpMenuButton from './MpMenuButton';

export interface IProps {
    navigation: any;
    title: any;
    subtitle?: any;
    rightComponent?: any;
}

interface IState {
    loading: boolean;
}

class Index extends Component<IProps, IState> {

    render() {

        const {rightComponent, title, subtitle, navigation} = this.props;
        const isParent = navigation.isFirstRouteInParent();

        return (
            <Header
                barStyle='dark-content'
                statusBarProps={{
                    backgroundColor: white
                }}
                leftComponent={
                    isParent
                        ? <MpMenuButton/>
                        : <MpBackButton/>
                }
                centerComponent={
                    <View style={styles.text}>
                        <Text style={styles.title}>
                            {title}
                        </Text>
                        {
                            subtitle &&
                            <Text style={styles.subtitle}>
                                {subtitle}
                            </Text>
                        }
                    </View>
                }
                rightComponent={rightComponent}
                containerStyle={styles.container}
            />
        );
    }
}

export default withNavigation(Index);

const styles = StyleSheet.create({
    container: {
        backgroundColor: white,
        marginTop: Platform.select({android: -24, ios: 0}),
        paddingLeft: 10,
        paddingRight: 10,
        borderBottomWidth: 0
    },
    text: {
        ...baseStyles.center,
    },
    title: {
        ...baseStyles.text,
        fontSize: 20
    },
    subtitle: {
        ...baseStyles.text,
        fontSize: 13
    }
});
