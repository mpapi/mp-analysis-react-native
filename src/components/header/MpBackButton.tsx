import React, {Component} from 'react';
import {TouchableOpacity} from 'react-native';
import {Image} from 'react-native-elements';
import {withNavigation} from 'react-navigation';

import {headerButtonStyle} from '../../styles';

export interface IProps {
    navigation: any;
}

class MpBackButton extends Component<IProps> {

    back = () => {
        const {navigation} = this.props;
        navigation.goBack();
    };

    render() {
        return (
            <TouchableOpacity
                activeOpacity={1}
                style={headerButtonStyle}
                onPress={this.back}
            >
                <Image
                    source={require('./../../assets/icon/Back.png')}
                />
            </TouchableOpacity>
        );
    }
}

export default withNavigation(MpBackButton);
