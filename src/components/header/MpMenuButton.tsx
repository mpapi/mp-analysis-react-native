import React, {Component} from 'react';
import {TouchableOpacity} from 'react-native';
import {Image} from 'react-native-elements';
import {withNavigation} from 'react-navigation';

import {headerButtonStyle} from '../../styles';

export interface IProps {
    navigation: any;
}

class MpMenuButton extends Component<IProps> {

    openDrawer = () => {
        const {navigation} = this.props;
        navigation.openDrawer();
    };

    render() {

        return (
            <TouchableOpacity
                activeOpacity={1}
                style={headerButtonStyle}
                onPress={this.openDrawer}
            >
                <Image
                    source={require('./../../assets/icon/Menu.png')}
                />
            </TouchableOpacity>
        );
    }
}

export default withNavigation(MpMenuButton);
