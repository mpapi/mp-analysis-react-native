import _ from 'lodash';
import React, {Component} from 'react';
import {Input} from 'react-native-elements';

import {inputContainerStyle, inputErrorStyle, inputLabelStyle, inputValueContainerStyle, inputValueStyle} from '../../styles';

export interface IProps {
    errorMessage?: string;
    [key: string]: any;
}

interface IState {
    loading: boolean;
}

class MpInput extends Component<IProps, IState> {

    shouldComponentUpdate(nextProps: Readonly<IProps>, nextState: Readonly<IState>, nextContext: any): boolean {
        const isChangeProps = !_.isEqual(nextProps, this.props);
        const isChangeState = !_.isEqual(nextState, this.state);

        return isChangeState || isChangeProps;
    }

    render() {
        const {errorMessage, ...otherProps} = this.props;
        return (
            <Input
                containerStyle={{...inputContainerStyle(!!errorMessage)}}
                errorMessage={errorMessage}
                inputContainerStyle={inputValueContainerStyle}
                inputStyle={inputValueStyle}
                labelStyle={inputLabelStyle}
                errorStyle={inputErrorStyle}
                numberOfLines={1}
                shake={true}
                {...otherProps}
            />
        );
    }
}

export default MpInput;
