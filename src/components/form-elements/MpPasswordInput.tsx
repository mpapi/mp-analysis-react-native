import React, {Component} from 'react';

import MpInput, {IProps as IBaseInputProps} from './MpInput';

export interface IProps extends IBaseInputProps {
}

class MpPasswordInput extends Component<IProps> {

    render() {
        return (
            <MpInput
                secureTextEntry={true}
                autoCapitalize='none'
                {...this.props}
            />
        );
    }
}

export default MpPasswordInput;
