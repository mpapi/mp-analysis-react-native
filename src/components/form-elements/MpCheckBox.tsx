import _ from 'lodash';
import React, {Component} from 'react';
import {CheckBox} from 'react-native-elements';

import {inputContainerStyle} from '../../styles';

export interface IProps {
    checked?: boolean;
    onChange?: any;
    errorMessage?: string;

    [key: string]: any;
}

interface IState {
    loading: boolean;
}

class MpCheckBox extends Component<IProps, IState> {

    shouldComponentUpdate(nextProps: Readonly<IProps>, nextState: Readonly<IState>, nextContext: any): boolean {
        const isChangeProps = !_.isEqual(nextProps, this.props);
        const isChangeState = !_.isEqual(nextState, this.state);

        return isChangeState || isChangeProps;
    }

    onPress = () => {
        const {onChange, checked} = this.props;

        onChange(!checked);
    };

    render() {
        const {checked, errorMessage, ...otherProps} = this.props;
        return (
            <CheckBox
                containerStyle={{...inputContainerStyle(!!errorMessage)}}
                checked={!!checked}
                onPress={this.onPress}
                {...otherProps}
            />
        );
    }
}

export default MpCheckBox;
