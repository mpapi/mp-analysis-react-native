import _ from 'lodash';
import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {Text} from 'react-native-elements';

import withScreenContext from '../../contexts/withScreenContext';
import {SelectEntity} from '../../entity/SelectEntity';
import {inputErrorStyle, inputIconContainerStyle, inputLabelStyle, inputValueContainerStyle, inputValueStyle, margin} from '../../styles';
import MpIcon from '../icon/MpIcon';
import {MpLoader} from '../loader';
import MpSearchOverlay from '../search-overlay';

export interface IProps {
    errorMessage?: string;
    search: boolean;
    loading: boolean;
    data: SelectEntity[];
    value: any;
    onChange?: ({item, index}: { item: SelectEntity, index: number }) => void;
    onChangeValue?: (value: any) => void;

    [key: string]: any;
}

interface IState {
    isVisible: boolean;
    searchText: string;
    data: SelectEntity[];
}

class MpSelect extends Component<IProps, IState> {

    state = {
        isVisible: false,
        searchText: '',
        data: this.props.data
    };

    onSelect = ({item, index}: { item: SelectEntity, index: number }) => {
        const {onChangeValue, onChange} = this.props;

        if (!_.isUndefined(onChangeValue)) {
            onChangeValue(item.value);
        }

        if (!_.isUndefined(onChange)) {
            onChange({item, index});
        }

        this.setState({isVisible: false});
    };

    onClose = () => {
        this.setState({isVisible: false});
    };

    shouldComponentUpdate(nextProps: Readonly<IProps>, nextState: Readonly<IState>, nextContext: any): boolean {
        const isChangeProps = !_.isEqual(nextProps, this.props);
        const isChangeState = !_.isEqual(nextState, this.state);

        // data props değişince state içerisindeki data da değişmeli.
        if (!_.isEqual(nextProps.data, this.props.data)) {
            this.setState({data: nextProps.data});
        }

        return isChangeState || isChangeProps;
    }

    render() {
        const {errorMessage, label, loading, search, value} = this.props;
        const {isVisible, data} = this.state;

        const item = !_.isUndefined(value) && _.find(data, {value});

        return (
            <>
                <View
                    style={{...margin('10'), marginBottom: errorMessage ? 3 : 25}}
                >
                    <Text
                        style={inputLabelStyle}
                    >
                        {label}
                    </Text>
                    <TouchableOpacity
                        style={styles.valueContainer}
                        activeOpacity={1}
                        onPress={() => this.setState({isVisible: true})}
                    >
                        <Text
                            style={styles.value}
                            numberOfLines={1}
                        >
                            {item && item.text}
                        </Text>

                        <View style={inputIconContainerStyle}>
                            {
                                loading
                                    ? <MpLoader/>
                                    : <MpIcon
                                        name='keyboard_arrow_down'
                                        size={20}
                                    />
                            }
                        </View>
                    </TouchableOpacity>
                    {
                        errorMessage &&
                        <Text
                            style={inputErrorStyle}
                        >
                            {errorMessage}
                        </Text>
                    }
                </View>
                <MpSearchOverlay
                    loading={loading}
                    isVisible={isVisible}
                    search={search}
                    onChange={this.onSelect}
                    onBackdropPress={this.onClose}
                    data={data}
                />
            </>
        );
    }
}

export default withScreenContext(MpSelect);

const styles = StyleSheet.create({
    valueContainer: {
        ...inputValueContainerStyle,
        flexDirection: 'row',
        alignItems: 'center',
    },
    value: {
        flex: 1,
        ...inputValueStyle
    },
});
