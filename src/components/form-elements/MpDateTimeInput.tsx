import _ from 'lodash';
import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import DatePicker from 'react-native-datepicker';
import {Text} from 'react-native-elements';

import {accent, inputErrorStyle, inputIconContainerStyle, inputLabelStyle, inputValueContainerStyle, inputValueStyle, margin, red} from '../../styles';
import MpIcon from '../icon/MpIcon';
import {MpLoader} from '../loader';

export enum MpDateTimeType {
    DATE = 'date',
    TIME = 'time',
    DATETIME = 'datetime'
}

export interface IProps {
    type: MpDateTimeType;
    errorMessage?: string;
    value: any;
    onChangeDate?: (value: any) => void;

    [key: string]: any;
}

export class MpDateTimeInput extends Component<IProps> {

    ref: any;

    getFormat = (type: MpDateTimeType) => {
        switch (type) {
            case 'date':
                return 'YYYY-MM-DD';
            case 'time':
                return 'HH:mm:ss';
            case 'datetime':
            default:
                return 'YYYY-MM-DD HH:mm:ss';
        }
    };

    onDateChange = (value: string) => {
        const {onChangeDate} = this.props;

        if (!_.isUndefined(onChangeDate)) {
            onChangeDate(value);
        }
    };

    shouldComponentUpdate(nextProps: Readonly<IProps>, nextState: Readonly<any>, nextContext: any): boolean {
        const isChangeProps = !_.isEqual(nextProps, this.props);
        const isChangeState = !_.isEqual(nextState, this.state);

        return isChangeState || isChangeProps;
    }

    render() {
        const {type, errorMessage, label, loading, value} = this.props;

        const format = this.getFormat(type);

        return (
            <>
                <View
                    style={{...margin('10'), marginBottom: errorMessage ? 3 : 25}}
                >
                    <Text
                        style={inputLabelStyle}
                    >
                        {label}
                    </Text>
                    <TouchableOpacity
                        style={styles.valueContainer}
                        activeOpacity={1}
                        onPress={() => this.ref.onPressDate()}
                    >
                        <Text
                            style={styles.value}
                            numberOfLines={1}
                        >
                            {value}
                        </Text>

                        <View style={inputIconContainerStyle}>
                            {
                                loading
                                    ? <MpLoader/>
                                    : <MpIcon
                                        style={inputIconContainerStyle}
                                        name='keyboard_arrow_down'
                                        size={20}
                                    />
                            }
                        </View>
                    </TouchableOpacity>
                    {
                        errorMessage &&
                        <Text
                            style={inputErrorStyle}
                        >
                            {errorMessage}
                        </Text>
                    }
                </View>
                <DatePicker
                    ref={(r) => this.ref = r}
                    mode={type}
                    format={format}
                    locale={'tr'}
                    style={{
                        width: 0,
                        height: 0,
                        borderWidth: 0,
                    }}

                    confirmBtnText='Tamam'
                    cancelBtnText='İptal'
                    customStyles={{
                        dateInput: {
                            borderWidth: 0
                        },
                        btnTextConfirm: {
                            color: accent,
                        },
                        btnTextCancel: {
                            color: red,
                        }
                    }}
                    showIcon={false}
                    is24Hour={true}
                    date={this.props.value}
                    onDateChange={this.onDateChange}
                />
            </>
        );
    }
}

const styles = StyleSheet.create({
    valueContainer: {
        ...inputValueContainerStyle,
        flexDirection: 'row',
        alignItems: 'center',
    },
    value: {
        flex: 1,
        ...inputValueStyle
    },
});
