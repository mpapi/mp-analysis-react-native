import _ from 'lodash';
import React, {Component} from 'react';

import MpInput, {IProps as IBaseInputProps} from './MpInput';

export interface IProps extends IBaseInputProps {
    onChangeNumber: any;
}

class MpNumberInput extends Component<IProps> {

    isFloat = (value: string | number) => {
        return /[.,]/.test(value.toString());
    };

    onChangeNumber = (text: string) => {
        const {value, onChangeNumber} = this.props;

        try {

            if (!_.isEmpty(text)) {

                const num = this.isFloat(text)
                    ? parseFloat(text)
                    : parseInt(text, 10);

                return onChangeNumber(num);
            }

        } catch (e) {

        }

        return onChangeNumber(value);
    };

    render() {
        const {value, onChangeNumber, ...otherValue} = this.props;

        return (
            <MpInput
                keyboardType='numeric'
                autoCapitalize='none'
                value={value ? value.toString() : '0'}
                {...otherValue}
                onChangeText={this.onChangeNumber}
            />
        );
    }
}

export default MpNumberInput;
