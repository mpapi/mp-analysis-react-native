import _ from 'lodash';
import React, {Component} from 'react';
import {withNavigation} from 'react-navigation';

import withScreenContext from '../../contexts/withScreenContext';
import {QueryEntity} from '../../entity/QueryEntity';
import {SelectEntity} from '../../entity/SelectEntity';
import {ProcessService} from '../../services/api/process.service';

import MpSelect, {IProps as ISelectProps} from './MpSelect';

export interface IProps extends ISelectProps {
    processKey: string;
    queryItem: QueryEntity;

    [key: string]: any;
}

interface IState {
    loading: boolean;
    selectedItem: SelectEntity | undefined;
    data: SelectEntity[];
}

class MpProcessSelect extends Component<IProps, IState> {

    processService: ProcessService;

    state = {
        loading: true,
        selectedItem: new SelectEntity(),
        data: this.props.data
    };

    constructor(props: IProps) {
        super(props);

        this.processService = new ProcessService(props.navigation);
    }

    componentDidMount() {
        this.load();
    }

    load = async () => {
        const {processKey, queryItem} = this.props;

        try {
            const data = await this.processService.run(processKey, [{queryKey: queryItem.queryKey}]);
            // TODO datayı kontrol etmek lazım. uygulama patlıyor...

            const {textColumn, valueColumn} = queryItem.select;
            const mappedData = _.map(data, (item) => {

                return {
                    text: _.get(item, textColumn || 'text'),
                    value: _.get(item, valueColumn || 'value')
                };
            });

            this.setState({
                data: mappedData,
                loading: false
            });

        } catch (error) {
            console.log('error: ', error);
            this.setState({loading: false});
        }
    };

    render() {
        const {loading, data} = this.state;

        return (
            <MpSelect
                {...this.props}
                loading={loading}
                data={data}
            />
        );
    }
}

const MpComponent = withScreenContext(MpProcessSelect);

export default withNavigation(MpComponent);
