import React, {Component} from 'react';

import MpInput, {IProps as IBaseInputProps} from './MpInput';

export interface IProps extends IBaseInputProps {
}

class MpUrlInput extends Component<IProps> {

    render() {
        return (
            <MpInput
                keyboardType='url'
                autoCapitalize='none'
                {...this.props}
            />
        );
    }
}

export default MpUrlInput;
