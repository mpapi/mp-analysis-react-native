import React, {Component} from 'react';

import MpInput, {IProps as IBaseInputProps} from './MpInput';

export interface IProps extends IBaseInputProps {
}

class MpEmailInput extends Component<IProps> {

    render() {
        return (
            <MpInput
                keyboardType='email-address'
                autoCapitalize='none'
                {...this.props}
            />
        );
    }
}

export default MpEmailInput;
