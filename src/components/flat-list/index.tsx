import React, {Component} from 'react';
import {FlatList, RefreshControl} from 'react-native';

import {accent} from '../../styles';

export interface IProps<T extends any> {
    data?: any[];
    renderItem: any;
    refreshing?: boolean | undefined;
    onRefresh?: () => void;

    [key: string]: any;
}

const exampleData = [
    {
        title: 'Food & Beverages',
        subtitle: 'Dinner',
        rightText: '$ 850'
    },
    {
        title: 'Food & Beverages',
        subtitle: 'Dinner',
        rightText: '$ 850'
    },
    {
        title: 'Food & Beverages',
        subtitle: 'Dinner',
        rightText: '$ 850'
    },
    {
        title: 'Food & Beverages',
        subtitle: 'Dinner',
        rightText: '$ 850'
    },
    {
        title: 'Food & Beverages',
        subtitle: 'Dinner',
        rightText: '$ 850'
    },
];

export class MpFlatList<T> extends Component<IProps<T>> {

    static defaultProps = {
        data: exampleData,
        refreshing: false
    };

    keyExtractor = (item: T, index: number) => {
        const key = JSON.stringify(item).substr(0, 40);

        return `${key}-${index}`;
    };

    onRefresh = () => {
        console.log('cant bind refreshControl');
    };

    render() {
        const {data, ...otherProps} = this.props;

        return (
            <FlatList
                keyExtractor={this.keyExtractor}
                data={data || []}
                refreshControl={
                    <RefreshControl
                        colors={[accent]}
                        tintColor={accent}
                        refreshing={!!this.props.refreshing}
                        onRefresh={this.props.onRefresh || this.onRefresh}
                    />
                }
                {...otherProps}
            />
        );
    }
}
