import React, {Component} from 'react';
import {ListItem} from 'react-native-elements';

import baseStyles, {listItemContainerStyle} from '../../styles';

export interface IProps {
    title?: string;
    subtitle?: string;
    index: number;

    [key: string]: any;
}

export class MpListItem extends Component<IProps> {

    render() {
        const {index, ...otherProps} = this.props;

        return (
            <ListItem
                containerStyle={listItemContainerStyle}
                titleStyle={{...baseStyles.text, fontSize: 18}}
                titleProps={{numberOfLines: 1}}
                subtitleStyle={{...baseStyles.subtext, fontSize: 14}}
                subtitleProps={{numberOfLines: 1}}
                {...otherProps}
            />
        );
    }
}
