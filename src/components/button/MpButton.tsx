import React, {Component} from 'react';
import {Platform} from 'react-native';
import {Button} from 'react-native-elements';

import {accent} from '../../styles';
import {shadow} from '../../styles/shadow';

class MpButton extends Component<any> {

    render() {
        const {backgroundColor, buttonStyle, containerStyle, ...otherProps} = this.props;
        return (
            <Button
                containerStyle={{
                    ...shadow(Platform.select({android: 1, ios: 2}), backgroundColor || accent),
                    borderRadius: 4,
                    ...containerStyle,
                }}
                activeOpacity={.8}
                buttonStyle={{
                    height: 40,
                    borderRadius: 4,
                    backgroundColor: backgroundColor || accent,
                    ...buttonStyle
                }}
                {...otherProps}
            />
        );
    }
}

export default MpButton;
