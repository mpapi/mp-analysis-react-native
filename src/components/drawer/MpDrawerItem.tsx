import * as _ from 'lodash';
import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {Text} from 'react-native-elements';

import {DataService} from '../../services/data.service';
import {grey, paleGrey, primary} from '../../styles';
import {hasIcon} from '../icon/MpFontIcon';
import MpIcon from '../icon/MpIcon';

export interface IProps {
    title?: string;
    divider?: boolean;
    icon?: string;
    depth: number;
    child?: any[];
    categoryId?: string;
    navTo?: string;
    onPress?: any;
    onActive?: any;
}

// interface IState {
// user: UserEntity;
// categories: CategoryEntity[];
// }

class MpDrawerItem extends Component<IProps> {

    static defaultProps = {
        title: 'Menu List Item',
        divider: false,
        icon: null,
        depth: 0,
        child: [],
        categoryId: undefined,
        navTo: undefined,
        onPress: () => false,
        onActive: () => false
    };

    dataService: DataService;

    constructor(props: any) {
        super(props);

        this.dataService = new DataService();
    }

    onNavTo = async () => {
        const {navTo, categoryId, onPress} = this.props;

        onPress({navTo, categoryId});
    };

    render() {

        const {divider, title, depth, child, onActive, navTo, categoryId} = this.props;

        const isActive = onActive({navTo, categoryId});

        const icon = (this.props.icon || '').replace('mp-', '');

        return (
            <View>
                <TouchableOpacity
                    style={styles.container}
                    activeOpacity={.8}
                    onPress={this.onNavTo}
                >
                    <View
                        style={[styles.separator, {opacity: isActive ? 1 : 0}]}
                    />
                    <View style={{
                        width: 24 + 18 + (depth * 16) + 16
                    }}>
                        {
                            hasIcon(icon) &&
                            <MpIcon
                                containerStyle={{
                                    marginLeft: 18 + (depth * 16),
                                    marginRight: 16
                                }}
                                name={icon}
                                size={20}
                                color={isActive ? primary : grey}
                            />
                        }
                    </View>
                    <Text
                        style={[styles.text, {color: isActive ? primary : grey}]}
                    >
                        {title}
                    </Text>
                </TouchableOpacity>
                {
                    _.map(child, (item, i) => (
                        <MpDrawerItem
                                key={depth + ' ' + i}
                                {...this.props}
                                categoryId={item._id}
                                title={item.title}
                                icon={item.icon}
                                child={item.children}
                                depth={depth + 1}/>
                    ))
                }
                {
                    divider &&
                    <View
                        style={{
                            borderBottomWidth: 1,
                            borderBottomColor: paleGrey,
                        }}
                    />
                }
            </View>
        );
    }
}

export default MpDrawerItem;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: 24,
        marginTop: 9,
        marginBottom: 9
    },
    separator: {
        width: 4,
        height: 24,
        backgroundColor: primary
    },
    text: {
        fontSize: 16,
        fontFamily: 'Roboto-Regular'
    }
});
