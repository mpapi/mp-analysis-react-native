import * as _ from 'lodash';
import React, {PureComponent} from 'react';
import {View} from 'react-native';
import {NavigationEvents} from 'react-navigation';

import {CategoryEntity} from '../../entity/CategoryEntity';
import {UserEntity} from '../../entity/UserEntity';
import {CategoryService} from '../../services/api/category.service';
import {UserService} from '../../services/api/user.service';
import {DataService} from '../../services/data.service';
import {SessionService} from '../../services/session.service';
import baseStyles, {grey, margin, primary, white} from '../../styles';
import {MpListItem} from '../flat-list/MpListItem';
import MpIcon from '../icon/MpIcon';
import {MpScrollView} from '../scroll-view';

import MpDrawerItem from './MpDrawerItem';

export interface IProps {
    navigation: any;
    activeItemKey?: string;
    t: any;
}

interface IState {
    user: UserEntity;
    categories: CategoryEntity[];
    categoryLoading: boolean;
    authLoading: boolean;
    drawerMenu: any[];
    selectedItem: string;
}

const dashboardItem = {
    title: 'Dashboard',
    icon: 'dashboard',
    navTo: 'DashboardStack'
};

const tasksItem = {
    title: 'Görevler',
    icon: 'tasks',
    navTo: 'TaskStack'
};

const reportsItem = {
    title: 'Tüm Raporlar',
    icon: 'line-chart',
    navTo: 'SwitchCategory',
    categoryId: '',
    divider: true,
};

class MpDrawerMenu extends PureComponent<IProps, IState> {

    sessionService: SessionService;
    userService: UserService;
    dataService: DataService;
    categoryService: CategoryService;

    state = {
        user: new UserEntity(),
        categories: [],
        categoryLoading: true,
        authLoading: true,
        drawerMenu: [],
        selectedItem: 'DashboardStack',
    };

    constructor(props: any) {
        super(props);

        this.sessionService = SessionService.getInstance();
        this.userService = new UserService(props.navigation);
        this.dataService = new DataService();
        this.categoryService = new CategoryService(props.navigation);
    }

    didFocus = async () => {

        await this.dataService
            .clear(['categoryId'])
            .save();

        await this.onLoad();
        await this.onLoadCategories();
    };

    onLoad = async () => {

        const drawerMenu = [dashboardItem];

        const user = await this.sessionService.getUser();

        if (!!_.result(user, 'permission.mpAnalysis.showTasks')) {
            drawerMenu.push(tasksItem);
        }

        drawerMenu.push(reportsItem);

        return new Promise(resolve => {
            this.setState({drawerMenu, user, authLoading: false}, resolve);
        });
    };

    onLoadCategories = async () => {

        this.setState({categoryLoading: true}, async () => {

            try {
                const categories = await this.categoryService.list();

                this.setState({categories, categoryLoading: false});
            } catch (e) {
                this.setState({categoryLoading: false});
            }
        });
    };

    onReAuth = async () => {
        const {authLoading} = this.state;

        if (authLoading) {
            console.log('skip auth');
            return;
        }

        this.setState({authLoading: true}, async () => {

            try {

                await this.userService.auth();

                await this.onLoad();

            } catch (e) {

                // skip navigation
                console.log('/user/auth catch: ', e.message);

                this.setState({authLoading: false});
            }
        });

    };

    onPress = async ({navTo = '', categoryId = ''}: { navTo: string, categoryId?: string }) => {
        const selectedItem = _.isEmpty(categoryId) ? navTo : `${navTo}${categoryId}`;

        this.setState({selectedItem});

        await this.dataService
            .set('categoryId', categoryId)
            .save();

        this.props.navigation.navigate(navTo);
    };

    onActive = ({navTo = '', categoryId = ''}) => {
        const current = _.isEmpty(categoryId) ? navTo : `${navTo}${categoryId}`;

        return current === this.state.selectedItem;
    };

    render() {
        const {authLoading, categories, drawerMenu, user, categoryLoading} = this.state;

        const title = this.sessionService.getAvatarText();
        const fullname = this.sessionService.getFullnameOrUsername();
        const email = user && user.email;

        return (
            <View style={[baseStyles.container, baseStyles.statusBarSpace]}>
                <NavigationEvents
                    onDidFocus={this.didFocus}
                />
                <View>
                    <MpScrollView
                        refreshing={authLoading}
                        onRefresh={this.onReAuth}
                    >
                        <MpListItem
                            leftAvatar={{
                                title,
                                size: 64,
                                containerStyle: {
                                    ...margin(8),
                                    borderWidth: 2,
                                    borderColor: primary
                                },
                                titleStyle: {color: grey},
                                overlayContainerStyle: {backgroundColor: white}
                            }}
                            index={0}
                            title={fullname}
                            subtitle={email}
                            rightElement={
                                <MpIcon
                                    name='settings'
                                />
                            }
                            onPress={() => this.onPress({navTo: 'ProfileStack'})}
                        />

                        {
                            _.map(drawerMenu, (item: any, i: number) => (
                                <MpDrawerItem
                                    key={item.title + '-' + i}
                                    {...item}
                                    onPress={this.onPress}
                                    onActive={this.onActive}
                                />
                            ))
                        }
                    </MpScrollView>
                </View>
                <MpScrollView
                    refreshing={categoryLoading}
                    onRefresh={this.onLoadCategories}
                    style={{marginBottom: 24}}
                >
                    {
                        _.map(categories, (item: CategoryEntity, i: number) => (
                            <MpDrawerItem
                                key={i}
                                categoryId={item._id}
                                navTo='SwitchCategory'
                                title={item.title}
                                icon={item.icon}
                                child={item.children}
                                onPress={this.onPress}
                                onActive={this.onActive}
                            />
                        ))
                    }
                </MpScrollView>
            </View>
        );
    }
}

export default MpDrawerMenu;
