import React from 'react';
import {ActivityIndicator, View} from 'react-native';

import {accent} from '../../styles';

export interface IProps {
    size: number;
    style?: any;
}

export class MpLoader extends React.PureComponent<IProps> {

    static defaultProps = {
        size: 30
    };

    render() {
        const {size, style} = this.props;
        return (
            <View style={{
                width: size,
                height: size,
                justifyContent: 'center',
                alignItems: 'center',
                ...(style || {})
            }}>
                <ActivityIndicator color={accent}/>
            </View>
        );
    }
}
