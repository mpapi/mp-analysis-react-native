import _ from 'lodash';
import React, {Component} from 'react';
import {View} from 'react-native';
import {SearchBar} from 'react-native-elements';

import withScreenContext from '../../contexts/withScreenContext';
import {SelectEntity} from '../../entity/SelectEntity';
import baseStyles, {inputValueContainerStyle, inputValueStyle, padding, white} from '../../styles';
import {MpFlatList} from '../flat-list';
import {MpListItem} from '../flat-list/MpListItem';
import KeyboardShift from '../KeyboardShift/KeyboardShift';
import {MpLoader} from '../loader';
import MpOverlay from '../overlay';

export interface IProps {
    isVisible: boolean;
    loading: boolean;
    search: boolean;
    data: SelectEntity[];
    onChange?: ({item, index}: { item: SelectEntity, index: number }) => void;
    onChangeValue?: (value: any) => void;

    [key: string]: any;
}

interface IState {
    searchText: string;
    data: SelectEntity[];
}

class MpSearchOverlay extends Component<IProps, IState> {

    state = {
        searchText: '',
        data: this.props.data
    };

    keyExtractor = (item: SelectEntity, index: number) => `${item.value}-${index}`;

    renderListItem = ({item, index}: { item: SelectEntity, index: number }) => {
        return (
            <MpListItem
                title={item.text}
                index={index}
                onPress={this.onSelect({item, index})}
            />
        );
    };

    onChangeSearchText = (searchText: string) => {
        const regex = new RegExp(`${searchText}`, 'gi');

        const data = _.filter(this.props.data, (item) => regex.test(item.text));

        this.setState({searchText, data});
    };

    onSelect = ({item, index}: { item: SelectEntity, index: number }) => () => {
        const {onChangeValue, onChange} = this.props;

        if (!_.isUndefined(onChangeValue)) {
            onChangeValue(item.value);
        }

        if (!_.isUndefined(onChange)) {
            onChange({item, index});
        }
    };

    shouldComponentUpdate(nextProps: Readonly<IProps>, nextState: Readonly<IState>, nextContext: any): boolean {
        const isChangeProps = !_.isEqual(nextProps, this.props);
        const isChangeState = !_.isEqual(nextState, this.state);

        // data props değişince state içerisindeki data da değişmeli.
        if (!_.isEqual(nextProps.data, this.props.data)) {
            this.setState({data: nextProps.data});
        }

        return isChangeState || isChangeProps;
    }

    render() {
        const {isVisible, loading, search, height, onBackdropPress} = this.props;
        const {data, searchText} = this.state;

        return (
            <MpOverlay
                isVisible={isVisible}
                onBackdropPress={onBackdropPress}
                overlayStyle={{
                    height: height / 2,
                    ...padding('0')
                }}
            >

                <View style={baseStyles.container}>
                    <KeyboardShift>
                        {
                            loading
                                ? (
                                    <View style={[baseStyles.container, baseStyles.center]}>
                                        <MpLoader/>
                                    </View>
                                )
                                : <MpFlatList
                                    style={baseStyles.container}
                                    data={data}
                                    renderItem={this.renderListItem}
                                />
                        }
                        {
                            search &&
                            <SearchBar
                                lightTheme
                                round
                                value={searchText}
                                containerStyle={{
                                    backgroundColor: white,
                                    borderTopWidth: 0
                                }}
                                inputStyle={inputValueStyle}
                                inputContainerStyle={inputValueContainerStyle}
                                onChangeText={this.onChangeSearchText}
                            />
                        }
                    </KeyboardShift>
                </View>
            </MpOverlay>
        );
    }
}

export default withScreenContext(MpSearchOverlay);
