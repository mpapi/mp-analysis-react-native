import {createIconSetFromIcoMoon} from 'react-native-vector-icons';

import config from './selection.json';

export const MpFontIcon = createIconSetFromIcoMoon(config);

export const hasIcon = MpFontIcon.hasIcon;
