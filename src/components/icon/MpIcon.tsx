import React, {Component} from 'react';
import {Icon} from 'react-native-elements';

import {grey} from '../../styles';

class MpIcon extends Component<any> {

    render() {
        return (
            <Icon
                name='add'
                color={grey}
                {...this.props}
                type='MP-API'
            />
        );
    }
}

export default MpIcon;
