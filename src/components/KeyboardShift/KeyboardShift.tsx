import React, {Component} from 'react';
import {Animated, Dimensions, Keyboard, StyleSheet, TextInput, UIManager} from 'react-native';

const {State: TextInputState} = TextInput;

export default class KeyboardShift extends Component<any> {
    state = {
        shift: new Animated.Value(0),
    };

    keyboardDidShowSub: any;
    keyboardDidHideSub: any;

    componentDidMount() {
        this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', this.handleKeyboardDidShow);
        this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide);
    }

    componentWillUnmount() {
        this.keyboardDidShowSub.remove();
        this.keyboardDidHideSub.remove();
    }

    render() {
        const {backgroundColor, children} = this.props;
        const {shift} = this.state;
        return (
            <Animated.View style={
                [
                    styles.container,
                    {backgroundColor: backgroundColor || '#fff'},
                    {transform: [{translateY: shift}]}
                ]
            }
            >
                {children}
            </Animated.View>
        );
    }

    handleKeyboardDidShow = (event: any) => {
        const {height: windowHeight} = Dimensions.get('window');
        const keyboardHeight = event.endCoordinates.height;
        const currentlyFocusedField = TextInputState.currentlyFocusedField();
        UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {

            const gap = (windowHeight - keyboardHeight) - (pageY + height);

            if (gap >= 0) {
                return;
            }

            Animated.timing(
                this.state.shift,
                {
                    toValue: gap - 10,
                    duration: 30,
                    useNativeDriver: true,
                }
            ).start();
        });
    };

    handleKeyboardDidHide = () => {
        Animated.timing(
            this.state.shift,
            {
                toValue: 0,
                duration: 100,
                useNativeDriver: true,
            }
        ).start();
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        left: 0,
        position: 'absolute',
        top: 0,
        width: '100%'
    }
});
