import React, {Component} from 'react';
import {RefreshControl, ScrollView} from 'react-native';

import {accent} from '../../styles';

export interface IProps {
    refreshing?: boolean | undefined;
    onRefresh?: () => void;

    [key: string]: any;
}

export class MpScrollView extends Component<IProps> {

    static defaultProps = {
        refreshing: false,
    };

    scrollRef: any;

    onRefresh = () => {
        console.log('cant bind refreshControl');
    };

    scrollTo = () => {
        return this.scrollRef.scrollTo();
    };

    render() {
        const {refreshing, onRefresh, children, ...otherProps} = this.props;

        return (
            <ScrollView
                ref={ref => this.scrollRef = ref}
                refreshControl={
                    <RefreshControl
                        colors={[accent]}
                        tintColor={accent}
                        refreshing={!!refreshing}
                        onRefresh={onRefresh || this.onRefresh}
                    />
                }
                {...otherProps}
            >
                {children}
            </ScrollView>
        );
    }
}
