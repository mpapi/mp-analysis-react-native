import * as _ from 'lodash';
import React, {Component} from 'react';
import {View} from 'react-native';
import {Text} from 'react-native-elements';
import {VictoryArea, VictoryAxis, VictoryBar, VictoryChart, VictoryLegend, VictoryLine, VictoryScatter, VictoryTooltip, VictoryVoronoiContainer} from 'victory-native';

import {MpChartEntity} from '../../entity/MpChartEntity';
import {MpSeriesEntity} from '../../entity/MpSeriesEntity';
import {ColorService} from '../../services/color.service';
import baseStyles, {grey, lightGrey, primaryA1, radius, white} from '../../styles';
import {wait} from '../../utils/wait';

const axisColor = {fill: grey, stroke: grey};

export interface IProps {
    errorText?: string | boolean;
    containerSize: { width: number, height: number };
    chartEntity: MpChartEntity;
    data: any[];
    onTouch?: (isTouched: boolean) => void;
}

interface IState {
    series: any[];
    legend: any[];
    voronoiBlacklist: string[];
    loading: boolean;
    errorText: string | boolean;
}

export const chartPadding = {
    top: 40,
    bottom: 50,
    left: 10,
    right: 10
};

const axisStyle = {
    axis: {fill: 'transparent', stroke: 'transparent'},
    grid: {
        stroke: lightGrey,
        strokeWidth: 1
    },
    axisLabel: {...axisColor},
    ticks: {...axisColor},
    tickLabels: {...axisColor, fontSize: 15, padding: 5}
};

class MpChart extends Component<IProps, IState> {

    static defaultProps = {
        onTouch: (isTouched: boolean) => {
            console.log('Is not binding. isTouched:', isTouched);
        }
    };

    colorService: ColorService;

    state = {
        loading: true,
        errorText: false,
        series: [],
        legend: [],
        voronoiBlacklist: [],
    };

    constructor(props: any) {
        super(props);
        this.colorService = new ColorService();
    }

    componentDidMount() {
        this.load();
    }

    load = () => {
        const {chartEntity, data, errorText} = this.props;

        if (errorText) {
            return this.setState({loading: false, errorText});
        }

        if (!data || data.length === 0) {
            return this.setState({loading: false, errorText: 'Veri bulunamadı!'});
        }

        if (!chartEntity || (!chartEntity.series || chartEntity.series.length === 0)) {
            return this.setState({loading: false, errorText: 'Seri ayarları bulunamadı!'});
        }

        this.setState({loading: true}, async () => {

            // çalışan didMound kodları varsa kısa süre bekleyerek performansı buradaki işlem hızını artırmak
            await wait(500);

            const allSeries = this.props.chartEntity.series;

            const series: any[] = [];
            const legend: any[] = [];
            const voronoiBlacklist: string[] = [];

            _.forEach(allSeries, (seriesItem, index) => {
                const result = this.renderSeries({seriesItem, index});

                legend.push(...result.legend);
                series.push(...result.series);
                voronoiBlacklist.push(...result.voronoiBlacklist);
            });

            this.setState({series, legend, voronoiBlacklist, loading: false});
        });
    };

    renderSeries({seriesItem, index = 0}: { seriesItem: MpSeriesEntity, index: number }) {
        const {chartEntity, data} = this.props;
        const color = this.colorService.getColor(index);

        const seriesProps = {
            x: chartEntity.xColumnName,
            y: seriesItem.columnName,
            data
        };

        const legend = [{name: seriesItem.text, symbol: {fill: color}}];
        const series: any[] = [];
        const voronoiBlacklist: string[] = [];

        switch (seriesItem.type) {
            case 'spline-area':
            case 'area':
                series.push(
                    <VictoryArea
                        key={`area-${index}`}
                        {...seriesProps}
                        interpolation={seriesItem.type === 'area' ? 'linear' : 'catmullRom'}
                        style={{
                            data: {
                                fill: color,
                                stroke: color,
                                strokeWidth: 3
                            },
                            labels: {fill: color}
                        }}
                    />
                );
                break;
            case 'bar':
                series.push(
                    <VictoryBar
                        key={`bar-${index}`}
                        {...seriesProps}
                        cornerRadius={{...radius('2')}}
                        style={{
                            data: {
                                fill: color,
                                stroke: color,
                                fillOpacity: 0.28
                            },
                            labels: {fill: color}
                        }}
                    />
                );
                break;
            case 'line':
            case 'spline':
            default:
                voronoiBlacklist.push(`skip-${index}`);
                series.push(
                    <VictoryLine
                        key={`line-${index}`}
                        {...seriesProps}
                        interpolation={seriesItem.type === 'line' ? 'linear' : 'catmullRom'}
                        style={{
                            data: {
                                stroke: color,
                                strokeWidth: 3
                            },
                            labels: {fill: color}
                        }}
                    />,
                    <VictoryScatter
                        key={`scatter2-${index}`}
                        name={`skip-${index}`}
                        {...seriesProps}
                        style={{
                            data: {
                                fill: color,
                                stroke: color,
                                strokeWidth: 2
                            }
                        }}
                    />
                );
        }

        return {
            legend,
            series,
            voronoiBlacklist
        };
    }

    tickFormat = (t: any) => _.isNumber(t) && t > 1000 ? `${Math.round(t)}k` : t;

    labels = ({_y}: any) => {
        return `Değer: ${_y}`;
    };

    shouldComponentUpdate(nextProps: Readonly<IProps>, nextState: Readonly<IState>, nextContext: any): boolean {

        const isChangeProps = !_.isEqual(nextProps, this.props);
        const isChangeState = !_.isEqual(nextState, this.state);

        return isChangeProps || isChangeState;
    }

    componentDidUpdate(prevProps: Readonly<IProps>, prevState: Readonly<IState>, snapshot?: any): void {
        const isChangeProps = !_.isEqual(prevProps, this.props);

        if (isChangeProps) {
            this.load();
        }
    }

    renderText = (text: string | boolean, isError: boolean = false) => {
        const {containerSize} = this.props;

        const textStyle = isError
            ? baseStyles.errorText
            : baseStyles.text;

        return (
            <View style={[containerSize, baseStyles.center]}>
                <Text style={{
                    ...textStyle,
                    fontSize: 16
                }}>
                    {text}
                </Text>
            </View>
        );
    };

    onTouch = (isTouched: boolean) => () => {
        const {onTouch} = this.props;

        if (onTouch) {
            onTouch(isTouched);
        }
    };

    render() {
        const {containerSize} = this.props;
        const {errorText, legend, series, voronoiBlacklist, loading} = this.state;

        if (loading) {
            return this.renderText('Grafik hazırlanıyor...');
        }

        if (errorText) {
            return this.renderText(errorText, true);
        }

        if (legend.length === 0 || series.length === 0) {
            return this.renderText('Grafik bulunamadı!');
        }

        return (
            <VictoryChart
                padding={chartPadding}
                {...containerSize}
                containerComponent={
                    // @ts-ignore
                    <VictoryVoronoiContainer
                        onTouchStart={this.onTouch(true)}
                        onTouchEnd={this.onTouch(false)}
                        voronoiBlacklist={voronoiBlacklist}
                        voronoiDimension={'x'}
                        voronoiPadding={5}
                        labels={this.labels}
                        labelComponent={
                            <VictoryTooltip
                                cornerRadius={0}
                                flyoutStyle={{
                                    fill: white,
                                    stroke: primaryA1,
                                    strokeWidth: 2
                                }}
                            />
                        }
                    />
                }
            >

                <VictoryLegend
                    x={10}
                    y={10}
                    orientation='horizontal'
                    symbolSpacer={10}
                    gutter={10}
                    data={legend}
                />

                <VictoryAxis
                    tickFormat={this.tickFormat}
                    style={axisStyle}
                />
                {series}
            </VictoryChart>
        );
    }
}

export default MpChart;
