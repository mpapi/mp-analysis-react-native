import React, {Component} from 'react';
import {View} from 'react-native';
import {Overlay} from 'react-native-elements';

import withScreenContext from '../../contexts/withScreenContext';
import {SelectEntity} from '../../entity/SelectEntity';
import baseStyles, {padding} from '../../styles';

export interface IProps {
    isVisible: boolean;
    onChange?: ({item, index}: { item: SelectEntity, index: number }) => void;
    onChangeValue?: (value: any) => void;

    [key: string]: any;
}

class MpOverlay extends Component<IProps> {

    render() {
        const {width, height, children, ...otherProps} = this.props;

        return (
            <Overlay
                width={width - 40}
                overlayStyle={{
                    height: height / 2,
                    ...padding('0')
                }}
                {...otherProps}
            >
                <View style={baseStyles.container}>
                    {children}
                </View>
            </Overlay>
        );
    }
}

export default withScreenContext(MpOverlay);
